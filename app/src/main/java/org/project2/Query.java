package org.project2;

import org.online_table.TableWeb;
import org.online_table.UserSample;
import org.project3.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Debug;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Query extends Activity implements OnClickListener {
	/** Called when the activity is first created. */

	public static String Score;
	private int worst;
	private int score;
	private String name;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.query);
		Button OkButton = (Button) findViewById(R.id.OK);
		Button SaveOnlineButton = (Button) findViewById(R.id.SaveOnline);
		EditText nameEditText = (EditText) findViewById(R.id.text);
		// ���������� ������ � �������� ��������
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(this);
		worst = settings.getInt("bad1", 100);
		Bundle extras = getIntent().getExtras();
		score = extras.getInt(Score);

		String Name= settings.getString("prevUser", "");  //last login
		nameEditText.setText(Name);

		//if (score <= worst)
		  // finish();
		OkButton.setOnClickListener(this);
		SaveOnlineButton.setOnClickListener(this);
	}

	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.OK: {
				EditText nameEditText = null;
				Intent intent = new Intent();
				nameEditText = (EditText) findViewById(R.id.text);
				name = nameEditText.getText().toString();
				intent.setClass(this, TableRecords.class);
				intent.putExtra(TableRecords.user, name);
				intent.putExtra(TableRecords.scores, score);
				finish();
				startActivity(intent);
				break;

			}

			case R.id.SaveOnline: {
                EditText nameEditText = null;
				Intent intent = new Intent();
				nameEditText = (EditText) findViewById(R.id.text);
                name = nameEditText.getText().toString();
                Log.d("Query", name);
                intent.setClass(this, org.online_table.UserSample.class);
				intent.putExtra(UserSample.user, name);
				intent.putExtra(UserSample.Score, score);
                //intent.putExtra(TableWeb.Mode, 1);
                finish();
				startActivity(intent);
				break;
			}
			default:
				break;
		}
	}

}