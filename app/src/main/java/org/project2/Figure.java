package org.project2;

import java.util.Random;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import android.util.Log;

public class Figure {


    public int[][] F;
    public static Random random = new Random();
	public static Random random2 = new Random();
	public int CubeSize;
	public int Color;
    private int mStatus = 0; //0-falling, //1-ready to get new one
	int numFig = 0;
	Bitmap next;
    SharedPreferences prefs;
	private Context mCont;

	public void renderT() {
        F[0][0] = 4;
        F[0][1] = 1;
        F[1][0] = 5;
        F[1][1] = 1;
        F[2][0] = 5;
        F[2][1] = 0;
        F[3][0] = 6;
        F[3][1] = 1;
    }

	public void renderS() {
        F[0][0] = 4;
        F[0][1] = 0;
        F[1][0] = 5;
        F[1][1] = 0;
        F[2][0] = 5;
        F[2][1] = 1;
        F[3][0] = 6;
        F[3][1] = 1;
	}

	public void renderR() {
        F[0][0] = 6;
        F[0][1] = 0;
        F[1][0] = 5;
        F[1][1] = 0;
        F[2][0] = 5;
        F[2][1] = 1;
        F[3][0] = 4;
        F[3][1] = 1;

	}

	public void renderL() {
        F[0][0] = 5;
        F[0][1] = 0;
        F[2][0] = 5;
        F[2][1] = 2;
        F[1][0] = 5;
        F[1][1] = 1;
        F[3][0] = 6;
        F[3][1] = 2;
   	}

	public void renderU() {
        F[0][0] = 5;
        F[0][1] = 0;
        F[2][0] = 5;
        F[2][1] = 2;
        F[1][0] = 5;
        F[1][1] = 1;
        F[3][0] = 4;
        F[3][1] = 2;

	}

	public void renderO() {
        F[0][0] = 6;
        F[0][1] = 0;
        F[1][0] = 5;
        F[1][1] = 0;
        F[2][0] = 5;
        F[2][1] = 1;
        F[3][0] = 6;
        F[3][1] = 1;
	}

	public void renderI() {
        F[0][0] = 5;
        F[0][1] = 0;
        F[1][0] = 5;
        F[1][1] = 1;
        F[2][0] = 5;
        F[2][1] = 2;
        F[3][0] = 5;
        F[3][1] = 3;
	}

    public  void setStatus(int status) {
        mStatus = status;
    }

    public int getStatus() {
        return mStatus;
    }

	public void rotate() {
        int x1 =  F[1][0] - F[0][0];
        int y1 = F[1][1] - F[0][1];
        int x2 =  F[1][0] - F[2][0];
        int y2 = F[1][1] - F[2][1];
        int x3 =  F[1][0] - F[3][0];
        int y3 = F[1][1] - F[3][1];
        int z1 =  F[1][0], z2 = F[1][1];
        F[0][0] = y1 + z1;
        F[0][1] = -x1 + z2;
        F[2][0] = y2 + z1;
        F[2][1]= -x2 + z2;
        F[3][0] = y3 + z1;
        F[3][1] = -x3 + z2;
	}

	public void copy(Figure F2) {
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 2; j++) {
                F[i][j] = F2.F[i][j];
            }
        }
		Color = F2.Color;
		CubeSize = F2.CubeSize;
	}


    public void saveFigure() {
        SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(mCont);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("numSimpleFig", numFig);
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 2; j++) {
                editor.putInt("SimpleFig" + i + "" + j , F[i][j]);
            }
        }
        editor.commit();
    }


    public void restoreFigure() {
        int check = 0;
        numFig = prefs.getInt("numSimpleFig", 0);
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 2; j++) {
                F[i][j] = prefs.getInt("SimpleFig" + i + "" + j, 0);
                Log.d("Dima", "restore fig" + F[i][j] + " " + i + " " + j);
                check = F[i][j];
            }
        }
        if(check == 0) {
            numFig = Math.abs(random.nextInt()) % 7;
            renderFigure();
            setNext();
        } else {
            setNext();
        }

    }

    public void renderFigure() {
        switch (numFig) {
            case (0):
                renderT();
                 break;
            case (1):
                renderS();
                break;
            case (2):
                renderR();
                break;
            case (3):
                renderI();
                break;
            case (4):
                renderO();
                break;
            case (5):
                renderU();
                break;
            case (6):
                renderL();
                break;
        }
    }


    public void setNext() {
        switch (numFig) {
            case (0):
                next = BallFactory.next_t;
                break;
            case (1):
                next = BallFactory.next_r;
                break;
            case (2):
                next = BallFactory.next_s;
                break;
            case (3):
                next = BallFactory.next_i;
                break;
            case (4):
                next = BallFactory.next_o;
                break;
            case (5):
                next = BallFactory.next_u;
                break;
            case (6):
                next = BallFactory.next_l;
                break;
        }
    }


    public Figure(int Size, Context context) {
		F = new int[4][2];
		mCont = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(mCont);
		Resources res = context.getResources();
		numFig = Math.abs(random.nextInt()) % 7;
		Color = Math.abs(random2.nextInt()) % 6;
		CubeSize = Size;
        if(prefs.getInt("needRefresh", 0) == 1) {
            restoreFigure();
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("needRefresh", 0);
            editor.commit();
        } else {
            renderFigure();
            setNext();
        }
	}

	public void paint(Canvas canvas, Paint mPaint) {
		Bitmap mImage1;
		mImage1 = BallFactory.blackImage;
		int numTheme = Integer// Number of old theme
				.valueOf(prefs.getString("ballThemes", "6"));
		if (numTheme == 6) {
			switch (Color) {
			case (0):
				mImage1 = BallFactory.blackImage;
				break;
			case (1):
				mImage1 = BallFactory.purpleImage;
				break;
			case (2):
				mImage1 = BallFactory.orangeImage;
				break;
			case (3):
				mImage1 = BallFactory.redImage;
				break;
			case (4):
				mImage1 = BallFactory.yellowImage;
				break;
			case (5):
				mImage1 = BallFactory.blueImage;
				break;
			}
		} else {
			switch (numTheme) {
			case (0):
				mImage1 = BallFactory.blackImage;
				break;
			case (1):
				mImage1 = BallFactory.blueImage;
				break;
			case (2):
				mImage1 = BallFactory.orangeImage;
				break;
			case (3):
				mImage1 = BallFactory.redImage;
				break;
			case (4):
				mImage1 = BallFactory.yellowImage;
				break;
			case (5):
				mImage1 = BallFactory.purpleImage;
				break;
			}
		}

        for(int i = 0; i < 4; i++) {
            canvas.drawBitmap(mImage1, F[i][0] * CubeSize, F[i][1] * CubeSize, null);
        }
	}

	public void paint_next(Canvas canvas, Paint mPaint, int w, int h) {
		int s = w / 3 * 3;
		if (h / 4 < s)
			s = h / 4;
		next = Bitmap.createScaledBitmap(next, s, s, true);
		canvas.drawBitmap(next, (w - s) / 2, 3 * h / 8 + 40, null);

	}

	public void set(int s) {
		CubeSize = s;
	}

	public void down() {
        for(int i = 0; i < 4; i++) {
            F[i][1]++;
        }
	}

	public void up() {
        for(int i = 0; i < 4; i++) {
            F[i][1]--;
        }
	}

	public void right() {
        for(int i = 0; i < 4; i++) {
            F[i][0]++;
        }
  	}

	public void left() {
        for(int i = 0; i < 4; i++) {
            F[i][0]--;
        }
	}

	public int maxY() {
		int max = F[0][1];
        for(int i = 1; i < 4; i++) {
            if(F[i][1] > max) {
                max = F[i][1];
            }
        }
		return max;
	}

	public int minY() {
        int min = F[0][1];
        for(int i = 1; i < 4; i++) {
            if(F[i][1] < min) {
                min = F[i][1];
            }
        }
		return min;
	}

	public int maxX() {
        int max = F[0][0];
        for(int i = 1; i < 4; i++) {
            if(F[i][0] > max) {
                max = F[i][0];
            }
        }
   		return max;
	}

	public int minX() {
        int min = F[0][0];
        for(int i = 1; i < 4; i++) {
            if(F[i][0] < min) {
                min = F[i][0];
            }
        }
        return min;
	}
}
