/**
 * 
 */
package org.project2;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import android.content.Context;
import android.webkit.WebView;

/**
 * @author GDM
 * 
 */
public class WebViewUtils {

	/**
	 * Default baseUrl for HTML WebViews
	 */
	private static final String BASE_URL = "file:///android_asset/";
	/**
	 * Default mimeType for HTML WebViews
	 */
	private static final String MIME_TYPE = "text/html";
	/**
	 * Default encoding for HTML WebViews
	 */
	private static final String ENCODING = "UTF-8";

	/**
	 * Loads supplied data to the targeted webView
	 * 
	 * @param webView
	 *            - targeted WebView
	 * @param body
	 *            - HTML code
	 */
	public static void loadData(WebView webView, String body) {
		webView.loadDataWithBaseURL(BASE_URL, body, MIME_TYPE, ENCODING, null);
	}



	public static boolean loadTemplateFile(Context context,
			StringBuilder builder, String fileName) {
		try {
			InputStream inp = context.getAssets().open(fileName);
			String NL = System.getProperty("line.separator");
			Scanner scanner = new Scanner(inp, ENCODING);
			try {
				while (scanner.hasNextLine()) {
					builder.append(scanner.nextLine() + NL);
				}
			} finally {
				scanner.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
