package org.project2;

/**
 * Created by dmitry on 09.06.17.
 */

public final class Constants {
    private Constants() {
        // restrict instantiation
    }

    public static final int pointForGravityLine = 15;
    public static final int pointUsualLine = 20;
    public static final int pointBonusLine = 25;
    public static final int speedInc = 40;
    public static final int speedOne = 100;
    public static final int speedMin = 50;
    public static final int speedCouldUp = 100;
    public static final int speedMax = 400;
    public static final int infinity = 100000;
    public static final int newLevelKoef = 250;
    public static final int maxAddPart = 5;
    public static final int MaxBonus = 4;
    public static final int MaxOne = 4;
    //condittions of speed reducing
    public static final int PointU = 3;
    public static final int PointN = 3;

    //interface
    public static final int AchieveDuration = 5000;
    public static final  int SuperFigureDuration = 4000;
    public static final int DelayFrame = 150;
    public static final long delayFire = 700; //delay between fire frames
    public static long delayMove  = 8000;
    public static long timePreview = 1000;
    public static double startScale = 0.1;
    public static long delayHard = 15000;  //delay to one ball
    public static long delayCoverScreen = 4000;

    public static int STATUS_FALLING = 0;
    public static int STATUS_NEW = 1;

}
