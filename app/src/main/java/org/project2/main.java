package org.project2;

import org.project3.R;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;

public class main extends Activity {
	/** Called when the activity is first created. */

	public static Vibrator v;
    //public static

    private ProgressDialog progressDialog;
    private StringBuilder sb;
	private StringBuilder sb_css;
    private Context mContext;
	private Resources mRes;
    private String[] mLevelsNames = null;
	Handler handler = new Handler() {

		public void handleMessage(Message msg) {

			Bundle b = msg.getData();

			int key = b.getInt("Type notification");
            Toast toast;
			LayoutInflater inflater;
			View layout;
            //ImageView image;
			WebView achievementView;
            TextView txtView;
            String summary = "";
            String colorAchieve = "#443dca";
            String colorSuperFigure = "#ca5d3d";
            String colorOneFigure = "#27f989";
            Window window;
            WindowManager.LayoutParams lp;
            StringBuilder sbInfo = new StringBuilder();
            final Dialog dialog = new Dialog(mContext);

            final Handler handler  = new Handler();
            final Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (dialog .isShowing()) {
                        dialog .dismiss();
                    }
                }
            };

            inflater = getLayoutInflater();
            layout = inflater.inflate(R.layout.toast_layout,
                    (ViewGroup) findViewById(R.id.toast_layout));

            achievementView = (WebView) layout.findViewById(R.id.achieve_webview);
            achievementView.setBackgroundColor(0x00000000);


            switch (key) {
			/*
			 * case (0): toast = Toast.makeText(getApplicationContext(),
			 * "Warning Super Figure!!", Toast.LENGTH_SHORT); toast.show();
			 * break;
			 */

            case 0:

                int mLevel = b.getInt("Level");
                sbInfo.append("Congratulations! Level " + mLevel + " is opened" + "</p>");
                sbInfo.append("<p class=\"achievement-name\">" + mLevelsNames[mLevel - 1] + "</p>");
                sbInfo.append(" </div>\n  </div>\n  </body>\n </html>");
                //beginning of ss
                sbInfo.append("<style>\n");
                sbInfo.append(".achievement-banner { background:");

                summary = sb.toString() + sbInfo.toString() + colorAchieve + sb_css.toString();
                Log.d("Dima", "summary " + summary);
                break;

            case 1:

				sbInfo.append(mRes.getString(R.string.Superfigure) + "</p>");
				sbInfo.append("<p class=\"achievement-name\"> </p>");
				sbInfo.append(" </div>\n  </div>\n  </body>\n </html>");
                //beginning of ss
                sbInfo.append("<style>\n");
                sbInfo.append(".achievement-banner { background:");
                summary = sb.toString() + sbInfo.toString() + colorSuperFigure + sb_css.toString();
                break;

             case 2:

                    sbInfo.append(mRes.getString(R.string.Onefigure) + "</p>");
                    sbInfo.append("<p class=\"achievement-name\"> </p>");
                    sbInfo.append(" </div>\n  </div>\n  </body>\n </html>");
                    //beginning of ss
                    sbInfo.append("<style>\n");
                    sbInfo.append(".achievement-banner { background:");
                    summary = sb.toString() + sbInfo.toString() + colorOneFigure  + sb_css.toString();
                    break;
             case 3:
                 sbInfo.append(mRes.getString(R.string.Chance) + "</p>");
                 sbInfo.append("<p class=\"achievement-name\">Reducing speed...</p>");
                 sbInfo.append(" </div>\n  </div>\n  </body>\n </html>");
                 //beginning of ss
                 sbInfo.append("<style>\n");
                 sbInfo.append(".achievement-banner { background:");
                 summary = sb.toString() + sbInfo.toString() + colorOneFigure  + sb_css.toString();
                 break;

             default:
                 break;

			}


            org.project2.WebViewUtils.loadData(achievementView, summary);
            window = dialog.getWindow();
            dialog.setContentView(layout);
            window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            window.setGravity(Gravity.CENTER_VERTICAL);
            //dialog.setCanceledOnTouchOutside(true);
            lp = new WindowManager.LayoutParams();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            dialog.show();
            window.setAttributes(lp);

            switch (key) {
                case 0:
                    handler.postDelayed(runnable, Constants.AchieveDuration);
                    break;
                case(1):
                    handler.postDelayed(runnable, Constants.SuperFigureDuration);
                    break;
                case(2):
                    handler.postDelayed(runnable, Constants.SuperFigureDuration);
                    break;
                case(3):
                    handler.postDelayed(runnable, Constants.SuperFigureDuration);
                    break;
                default:
                    break;
            }

		}

	};


  
	/**
	 * Invoked when the user selects an item from the Menu.
	 * 
	 * @param item
	 *            the Menu entry which was selected
	 * @return true if the Menu item was legit (and we consumed it), false
	 *         otherwise
	 */
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// main.setOnTouchListener(this);
		// requesting to turn the title OFF
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// making it full screen
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// set our MainGamePanel as the View
		//setContentView(R.layout.main);
		setContentView(R.layout.load);
		mContext = this;
        mRes = getResources();
        mLevelsNames = mRes.getStringArray(R.array.Levels);

        //BL = new BallFactory(this);
        progressDialog = ProgressDialog.show(this, "", "Load game..");
        progressDialog.setCancelable(true);
        sb = new StringBuilder();
        sb_css  = new StringBuilder();
        org.project2.WebViewUtils.loadTemplateFile(this, sb, "achievement.html");
        org.project2.WebViewUtils.loadTemplateFile(this, sb_css, "style_achieve.css");
		setContentView(new GameView(this, handler, progressDialog));
		/*
		 * new Thread(new Runnable() {
		 * 
		 * public void run() {
		 * 
		 * initFactory(); } }).start();
		 */
		// setContentView(R.layout.load);
	}



	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// ������������� ���������� ��� ��������� ���������
	}
	
	
    /*
	protected void onResume() {
		//super.onStop();  
		//Log.d("Dima", "Activity on Start");
	}
	*/
	
	
	protected void onPause() {
		  super.onPause(); 
		  GameView.getThread().pause();
	}
	
	/*
	protected void onRestart() {
		Log.d("Dima", "Activity on Restart");
	}
	*/
	
	
}
