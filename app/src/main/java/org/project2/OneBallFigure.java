package org.project2;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.preference.PreferenceManager;

import java.util.Random;

/**
 * Created by dmitry on 10.04.17.
 */

public class OneBallFigure {
    public int F[];
    public static Random random = new Random();
    public static Random random2 = new Random();
    public int CubeSize;
    public int Color;
    int numFig;
    Bitmap next;
    private Context mCont;

    public void render() {
        F[0] = 5;
        F[1] = 0;

    }


    public void rotate() {

    }

    public void copy(OneBallFigure F2) {
        F[0] = F2.F[0];
        F[1] = F2.F[1];
        Color = F2.Color;
        CubeSize = F2.CubeSize;
    }

    public OneBallFigure(int Size, Context context) {
        F = new int[2];
        mCont = context;
        Resources res = context.getResources();
        Color = Math.abs(random2.nextInt()) % 6;
        CubeSize = Size;
        render();
    }

    public void paint(Canvas canvas, Paint mPaint) {
        Bitmap mImage1;
        SharedPreferences prefs;
        mImage1 = BallFactory.blackImage;
        prefs = PreferenceManager.getDefaultSharedPreferences(mCont);
        int numTheme = Integer// Number of old theme
                .valueOf(prefs.getString("ballThemes", "6"));
        if (numTheme == 6) {
            switch (Color) {
                case (0):
                    mImage1 = BallFactory.blackImage;
                    break;
                case (1):
                    mImage1 = BallFactory.purpleImage;
                    break;
                case (2):
                    mImage1 = BallFactory.orangeImage;
                    break;
                case (3):
                    mImage1 = BallFactory.redImage;
                    break;
                case (4):
                    mImage1 = BallFactory.yellowImage;
                    break;
                case (5):
                    mImage1 = BallFactory.blueImage;
                    break;
            }
        } else {
            switch (numTheme) {
                case (0):
                    mImage1 = BallFactory.blackImage;
                    break;
                case (1):
                    mImage1 = BallFactory.blueImage;
                    break;
                case (2):
                    mImage1 = BallFactory.orangeImage;
                    break;
                case (3):
                    mImage1 = BallFactory.redImage;
                    break;
                case (4):
                    mImage1 = BallFactory.yellowImage;
                    break;
                case (5):
                    mImage1 = BallFactory.purpleImage;
                    break;
            }
        }
        canvas.drawBitmap(mImage1, F[0] * CubeSize, F[1] * CubeSize, null);

    }

    public void paint_next(Canvas canvas, Paint mPaint, int w, int h) {
        int s = w / 3 * 3;
        if (h / 4 < s)
            s = h / 4;
        next = Bitmap.createScaledBitmap(next, s, s, true);
        canvas.drawBitmap(next, (w - s) / 2, 3 * h / 8 + 40, null);

    }

    public void set(int s) {
        CubeSize = s;
    }

    public void down() {
        F[1]++;
    }

    public void up() {
        F[1]--;
    }

    public void right() {
        F[0]++;

    }

    public void left() {
        F[0]--;
     }

    public int maxY() {
        int max = F[1];
        return max;
    }

    public int minY() {
        int min = F[1];

        return min;
    }

    public int maxX() {
        int max = F[0];

        return max;
    }

    public int minX() {
        int min = F[0];
        return min;
    }
}
