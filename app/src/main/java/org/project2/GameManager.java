package org.project2;

import org.project3.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.SurfaceHolder;

import java.util.Random;

public class GameManager extends Thread {
	private interface DrawHelper {
		void draw(Canvas canvas);
	}

	private Context mCont;
    private SharedPreferences mSettings;
	// *******************************
    public static Random random = new Random();

	private boolean mRunning;
	public boolean mPause;      //[mod] 2016.06.18
	
	private boolean mInitialized = true;
	// ********************************

	// *********************************
	private SurfaceHolder mSurfaceHolder;
	private Paint mPaint;
	// ******************************
	static public BallFactory mFactory;
	// *****************************

	// ***********************
	private Falling mFall;
	private Figure mFig;
	private Figure mNextFig;
	private Figure mPredFig;
    private OneBallFigure mOneFig;
    private OneBallFigure mOnePred;
	private BonusFigure mBonFig;
    private BonusFigure mBonPredFig;
	// ***********************

	// *************************************
	private int mPoint = 0; // Game point
	public static int mDirection = 0; // Figure direction
	private int mNumRow = 0;
	private int mCountBonus = 0;      // Count of bonus figures
    private int mCountOneBall = 0;    // Count of one ball figures
	public static int mCurSpeed;      // Current speed
	private int mUsualSpeed = 300;
	private int mSpeedDown = 0;       // Speed for figure's fall
	private int mLevel = 1;
    private int mPrevLevel = 0; //Previous level for checking need of notifs
	public static int mSize;// Size of one figure's square
    public static int mSizeFreeSpace; //Size of free space between field and boarder phone
	private int mBonusMult = 1; //Multiplier to became bonus Figure further
    private int mOneMult = 1;
	private long time;
	private long timeFire;
	private long timeLaugh = System.currentTimeMillis(); //02.08.2016 time of laugh
    public static  long timeLastMove = 0; //time before last pressure

    public  static long timeBtnPress = 0;
    private int delayMove2 = 275;
    private long timeProblem = 0;//when too much lines. For checking neccesity of one balls

	private int mCurMaxBonus, mCurMaxOne; //QTY bonus and one balls
	private int mNewLevelBounds = Constants.newLevelKoef;
    private int mPrevLevelBound = 0;
    private int mNewBoundsBonus = mNewLevelBounds - Constants.pointUsualLine;
    private int mNewSpeedBound = (mNewLevelBounds + mPrevLevelBound) / 2;
    private int mRefreshFig = 1;       //if we need resfresh last figure
	private int mRefreshFalling = 1;   //if we need refresh  last falling

    // ****************************************
	Canvas cnvs;
	// **********************************

	private boolean flag_fire = false; // Lines if removed
	private int type_figure = 0; // Тип фигуры. 0 - simple, 1 - bonus, 2 - one ball
	private boolean flag_first = true;// thread is initialed firstly
	private boolean flag_mod = false;// Balls are falling by gravitation. Animation.
	private boolean flag_l = false;// Left button is not down
	private boolean flag_r = false;// Right button is not down
	private boolean flag_down = false;
	private boolean flag_rot = false;
	private boolean mVibration = false;//
	private boolean mSound = false;//
	private boolean flagClass = true;
	private boolean flagMusicPlaying = false;
    private boolean flagShowPointsInfo = false;
	private int flagSettingsChanged = 0;
    public static int mTypeControl = 0;
	private int timeLeft = 0;
    private int timeRight = 0;
	private int mExit = 1;
    private int mCurGoal = 1;



	// ******************************
	public static int FieldHeight, FieldWidth; // Sizes of game field
	private int mHeight;

	static public int mWidth;
	private Rect mField;
	private Rect mBounds;
	private Rect mRectPoint;
	private int mTextSize;
	private int[] mFallVect; // mFallVector of falling balls
	// ***********************************

	private int mNumLine = 0;// Number of removed line
	private int mNumImag = 0;
    public static int mFlagPressFig = 0;

	public static int pause_size;

	// **********************************
	Music music;
	// **********************************

	// ****************************************
	private DrawHelper drawObjects;
	private DrawHelper drawGameover;
	private DrawHelper drawPause;


	// ********************************************
	private static int BUTT_SIZE_MM = 9;
	private static int PAUSE_SIZE_MM = 6;
	public static int mButtSize;
	public static int mLeftButtBound;


	private Handler mHandler;
	private Handler mHandlerRight = new RefreshHandlerRight();
	private Handler mHandlerLeft = new RefreshHandlerLeft();;

	class RefreshHandlerRight extends Handler {
		@Override
		public void handleMessage(Message msg) {
			if (flag_r) {
				Vibrator vibrator = (Vibrator) mCont
						.getSystemService(Context.VIBRATOR_SERVICE);
				if (mVibration)
					vibrator.vibrate(20);
				//right();

				this.sleep(Constants.delayMove);
			}
		}

		public void sleep(long delayMillis) {
			this.removeMessages(0);
			//sendMessageDelayed(obtainMessage(0), delayMillis);
		}
	};

	class RefreshHandlerLeft extends Handler {
		@Override
		public void handleMessage(Message msg) {
			if (flag_l) {
				Vibrator vibrator = (Vibrator) mCont
						.getSystemService(Context.VIBRATOR_SERVICE);
				if (mVibration) {
                    vibrator.vibrate(20);
                }
				//left();
				this.sleep(Constants.delayMove);
			}
		}

		public void sleep(long delayMillis) {
			this.removeMessages(0);
			//sendMessageDelayed(obtainMessage(0), delayMillis);
		}
	};

	public GameManager(SurfaceHolder surfaceHolder, Context context,
			Handler handler, Falling fall) {
		long time_start = System.currentTimeMillis();
		//mFactory =  new BallFactory (context);
		mSurfaceHolder = surfaceHolder;
		mFall = fall;

		mRunning = false;
		mInitialized = false;
		mPause = false;
		mHandler = handler;
		mCont = context;
		Typeface tf =Typeface.createFromAsset(mCont.getAssets(),"fonts/Economica-Regular.ttf");

		mPaint = new Paint();
		mPaint.setStyle(Paint.Style.FILL);
		mPaint.setTypeface(tf);
		Resources res = context.getResources();
		mField = new Rect();
		mRectPoint = new Rect();

		mSettings = PreferenceManager
				.getDefaultSharedPreferences(context);


		// -----------------------------------------------------
		//
		drawGameover = new DrawHelper() {
			public void draw(Canvas canvas) {
				mPaint.setStyle(Paint.Style.FILL);
				mPaint.setColor(Color.YELLOW);
				mPaint.setTextSize(mTextSize);
				canvas.drawText("GAME OVER", mWidth / 2 - 70, mHeight / 2,
						mPaint);

				Intent intent = new Intent();
				intent.putExtra(Query.Score, mPoint);
				intent.setClass(mCont, Query.class);
				mUsualSpeed  = 300;
				mLevel = 1;
				mPoint = 0;
				mCountBonus = 1;
                mCountOneBall = 0;
                mRefreshFig = 0;
                mRefreshFalling = 0;
				mExit = 0;
                mCurGoal = 0;
                BallFactory.refreshImages();
				((Activity) mCont).finish();
				mCont.startActivity(intent);
			}
		};

		drawPause = new DrawHelper() {
			public void draw(Canvas canvas) {
				mPaint.setStyle(Paint.Style.FILL);
				mPaint.setColor(Color.YELLOW);
				mPaint.setTextSize(mTextSize);
				canvas.drawText("PAUSE", mWidth / 2 - 70, mHeight / 2, mPaint);
			}
		};

		drawObjects = new DrawHelper() {
			public void draw(Canvas canvas) {
                long time = System.currentTimeMillis();
                cnvs = canvas;

                canvas.drawBitmap(BallFactory.mImageWall, 0, 0, null);

                if (flag_r != true)
                    BallFactory.mImageRight.draw(cnvs);
                else
					BallFactory.mImageRightPress.draw(cnvs);
                if (flag_l != true)
					BallFactory.mImageLeft.draw(cnvs);
                else
					BallFactory.mImageLeftPress.draw(cnvs);
                if (flag_down) {
					BallFactory.mImageDownPress.draw(cnvs);
                    flag_down = false;
                } else {
					BallFactory.mImageDown.draw(cnvs);
                }
                if (flag_rot) {
					BallFactory.mImageRotatePress.draw(cnvs);
                    flag_rot = false;
                } else {
					BallFactory.mImageRotate.draw(cnvs);
                }
				// mNextFig.paint_next(canvas, mPaint, (mWidth - FieldWidth
				// * mSize) / 2, FieldHeight * mSize);
		   	    mPaint.setColor(Color.BLACK);

				// canvas.drawRect(mRectPoint, mPaint);
				canvas.save();
				canvas.translate((mWidth - FieldWidth * mSize) / 2, 0);
				mPaint.setColor(Color.BLACK);
				mPaint.setStyle(Paint.Style.STROKE);
				mPaint.setStrokeWidth(3);
				//canvas.drawRect(mField, mPaint);
				mPaint.setStyle(Paint.Style.FILL);
				drawScoreField(canvas, mPaint);
				mPaint.setStyle(Paint.Style.FILL);
				mPaint.setAntiAlias(true);
				long time_phone = System.currentTimeMillis();
				// if (flag_fire == false) {
				canvas.drawRect(mField, mPaint);
				// mImagePhone.setAlpha(25);

                canvas.drawBitmap(BallFactory.mImageBack, 0, 0, null);
                if(BallFactory.isAnimateGoal  == true) {
                    canvas.drawBitmap(BallFactory.mImageGoalFrame, (int) Math.round(FieldWidth * mSize * 0.14), (int) Math.round(FieldHeight * mSize * 0.227), null);
                }
                    // }
				time_phone = System.currentTimeMillis() - time_phone;
				long time_figure = System.currentTimeMillis();
				if (flag_mod == false) { //
					switch (type_figure) {
                        case (0):
                            mFig.paint(canvas, mPaint);
                            break;
                        case (1):
                            mBonFig.paint(canvas, mPaint);
                            break;
                        case (2):
                            mOneFig.paint(canvas, mPaint);
                            break;
                        default:
                            mFig.paint(canvas, mPaint);
                    }


				}
				time_figure = System.currentTimeMillis() - time_figure;
				long tim = System.currentTimeMillis();
				if (flag_fire == false) {
					mFall.paint(canvas, mPaint, mCont);
				} else {
					mFall.paintS(canvas, mPaint, mCont);
				}
				long time3 = System.currentTimeMillis() - tim;



				if ((-timeLaugh  + System.currentTimeMillis() > 1200) && (flagMusicPlaying == true)) {
                    Music.pause(mCont, R.raw.point3);
				   	flagMusicPlaying = false;
				}

                if(BallFactory.isAnimateGoal  == true) {
                   if(BallFactory.isAnimate == true) {
                       BallFactory.AnimatePreview((int) Math.round(FieldWidth * mSize * 0.7), (int) Math.round(FieldHeight * mSize * 0.2));
                   } else {
                       BallFactory.AnimateGoal(((int) Math.round(FieldWidth * mSize * 0.7)), (int) Math.round(FieldHeight * mSize * 0.2));
                   }
                }

				if (flag_fire == true) {
					Vibrator vibrator = (Vibrator) mCont
							.getSystemService(Context.VIBRATOR_SERVICE);
					if (mVibration)
						vibrator.vibrate(40);
					if((mSound) && (flagMusicPlaying  == false))  {
					   Music.play(mCont, R.raw.point3);
					   flagMusicPlaying = true;
					   timeLaugh = System.currentTimeMillis();
					}
					if (-timeFire + System.currentTimeMillis() < Constants.delayFire ) {
						mNumImag = (mNumImag + 1) % 3;


						for (int s2 = mFall.num; s2 >= mFall.num - mFall.getKoef() + 1; s2--) {
							for (int k = 0; k < FieldWidth; k++) {
								BallFactory.mImage[mNumImag]
										.setBounds(k * mFig.CubeSize, s2
												* mFig.CubeSize,
												k * mFig.CubeSize
														+ mFig.CubeSize, s2
														* mFig.CubeSize
														+ mFig.CubeSize);
								BallFactory.mImage[mNumImag].draw(canvas);
							}
						}
					} else {


						for (int i = 0; i < FieldWidth; i++)
							mFallVect[i] = 0;
						flag_mod = true;
						flag_fire = false;
						mNumRow = mFall.num;
					}
				}



				/*
				 * new Thread(new Runnable() { public void run() {
				 *
				 * if (flag_r != true) mImageRight.draw(cnvs); else
				 * mImageRightPress.draw(cnvs); if (flag_l != true)
				 * mImageLeft.draw(cnvs); else mImageLeftPress.draw(cnvs); if
				 * (flag_down) { mImageDownPress.draw(cnvs); flag_down = false; }
				 * else { mImageDown.draw(cnvs); } if (flag_rot) {
				 * mImageRotatePress.draw(cnvs); flag_rot = false; } else {
				 * mImageRotate.raw(cnvs); } } }).start();
				 */
				canvas.restore();
				long time_pause = System.currentTimeMillis();
				if (mPause) {
					BallFactory.mImagePauseStp.draw(canvas);
					mPaint.setStyle(Paint.Style.FILL);
					mPaint.setColor(Color.YELLOW);
					mPaint.setTextSize(mTextSize);
					canvas.drawText("PAUSE", mWidth / 2 - 70, mHeight / 2, mPaint);
				} else {
					BallFactory.mImagePausePl.draw(canvas);
				}
				BallFactory.mImageSettings.draw(canvas);
				time_pause = System.currentTimeMillis() - time_pause;
				time = System.currentTimeMillis() - time;
				cnvs = null;

			}

		};
	}

	private void drawScoreField(Canvas canvas, Paint paint) {
		Rect bounds = new Rect();
		String word;
		int offsetY = FieldHeight * mSize / 8;
		int off = (mWidth - FieldWidth * mSize) / 2;
		mPaint.setColor(Color.rgb(0, 10, 200));
		canvas.save();
		canvas.translate(-off, 0);

		mNextFig.paint_next(canvas, paint, off, FieldHeight * mSize);
		word = "Next";
		paint.getTextBounds(word, 0, word.length(), bounds);
		canvas.drawText(word, (off - bounds.width()) / 2, pause_size * 4, mPaint);
		canvas.restore();
		canvas.save();
		canvas.translate(FieldWidth * mSize, 0);
		mPaint.setColor(Color.rgb(255, 255, 255));
		mPaint.setTextSize(mTextSize);
		word = "Score";
		paint.getTextBounds(word, 0, word.length(), bounds);
		canvas.drawText(word, (off - bounds.width()) / 2, offsetY, mPaint);
		offsetY += FieldHeight * mSize / 8;
		word = "" + mPoint;
		paint.getTextBounds(word, 0, word.length(), bounds);
		canvas.drawText(word, (off - bounds.width()) / 2, offsetY, mPaint);
		offsetY += FieldHeight * mSize / 4;
		word = "Level";
		paint.getTextBounds(word, 0, word.length(), bounds);
		canvas.drawText(word, (off - bounds.width()) / 2, offsetY, mPaint);
		offsetY += FieldHeight * mSize / 8;
		word = "" + mLevel;
		paint.getTextBounds(word, 0, word.length(), bounds);
		canvas.drawText(word, (off - bounds.width()) / 2, offsetY, mPaint);
		offsetY += FieldHeight * mSize / 4;
		word = "Speed";
		paint.getTextBounds(word, 0, word.length(), bounds);
		canvas.drawText(word, (off - bounds.width()) / 2, offsetY, mPaint);
		offsetY += FieldHeight * mSize / 8;
		int s = 5 - mCurSpeed / 100;
		word = "" + s;
		paint.getTextBounds(word, 0, word.length(), bounds);
		canvas.drawText(word, (off - bounds.width()) / 2, offsetY, mPaint);
		offsetY += FieldHeight * mSize / 4;
		mPaint.setColor(Color.WHITE);
		canvas.restore();
	}

	private int getTextHeight(String text, Paint paint, int length) {
		Rect bounds = new Rect();
		int size = FieldHeight * mSize / 8;
		mPaint.setTextSize(FieldHeight * mSize / 8);
		paint.getTextBounds(text, 0, text.length(), bounds);
		int len = bounds.width();
		while (true) {
			if (len <= length)
				break;
			else {
				size--;
				mPaint.setTextSize(size);
				paint.getTextBounds(text, 0, text.length(), bounds);
				len = bounds.width();
			}
		}
		return size;
	}

	private void draw(DrawHelper helper) {
		Canvas canvas = null;
		try {
			// ���������� Canvas-�
			canvas = mSurfaceHolder.lockCanvas();
			synchronized (mSurfaceHolder) {
				if (mInitialized) {
					helper.draw(canvas);
				}

			}
		} catch (Exception e) {
		} finally {
			if (canvas != null) {
				mSurfaceHolder.unlockCanvasAndPost(canvas);
			}
		}
	}

	// Thread's state
	public void setRunning(boolean running) {
		mRunning = running;
	}



	public void updateStates() {
		// Update figure's state

           flagSettingsChanged = mSettings.getInt("Settings changed", 0);
           Log.d("Dima changed", flagSettingsChanged  + "");
           if(flagSettingsChanged == 1) {
               settingShanged();
               SharedPreferences sharedPref = PreferenceManager
                       .getDefaultSharedPreferences(mCont);
               SharedPreferences.Editor editor = sharedPref.edit();
               editor.putInt("Settings changed", 0);
               editor.commit();
           }
           switch (type_figure) {
                case (0):
                    updateSimple();
                    break;
                case (1):
                    updateBonus();
                    break;
                case (2):
                    updateOneBall();
                    break;
                default:
                    updateSimple();
            }
        if (System.currentTimeMillis() - time >= mCurSpeed) {
            time = System.currentTimeMillis();
            if ((flag_mod == false) && (flag_fire == false)) {
                switch (type_figure) {
                    case (0):
                        SimpleFigure();
                        break;
                    case (1):
                        BonusFigure();
                        break;
                    case (2):
                        OneBallFigure();
                        break;
                    default:
                        SimpleFigure();
                }
            }
        }
		if (flagClass == false) { //if type of game is not classic
			handleFall();  //create animation
		} else {
			flag_mod = false;
		}
	}
    /*
	public void updateSimple() {
		long timeForMove  = System.currentTimeMillis() - GameManager.timeBtnPress;
        Log.d("Dima", "time for move " + timeForMove);

        if(flgLeft + flgRight + flgRight > 0) {
            int flg_direct = 0;
            if(flgRight > 0) {
                flg_direct = 1;
            } else {
                if(flgLeft > 0) {
                    flg_direct = 2;
                } else {
                    flg_direct = 3;
                }
            }


            if(timeForMove > 100) {
                GameManager.timeBtnPress = System.currentTimeMillis();
                Figure F2 = new Figure(mSize, mCont);
                switch (flg_direct) {
                    case (1):
                        F2.copy(mFig);
                        mPredFig = new Figure(mSize, mCont);
                        mPredFig.copy(mFig);
                        F2.right();
                        if ((F2.maxX() <= FieldWidth - 1) && (!mFall.checkFall(F2))) {
                            mFig.right();
                        }
                        QTY_right--;
                        if((mDirection != 0) && (QTY_right == 0)) {
                            QTY_right++;
                        }
                        return;
                    case (2):

                        F2.copy(mFig);
                        F2.left();
                        mPredFig = new Figure(mSize, mCont);
                        mPredFig.copy(mFig);
                        if ((F2.minX() >= 0) && (!mFall.checkFall(F2))) {
                            mFig.left();
                        }
                        QTY_left--;
                        if((mDirection != 0) && (QTY_left == 0)) {
                            QTY_left++;
                        }
                        return;
                    case (3):
                        F2.copy(mFig);
                        F2.left();
                        mPredFig = new Figure(mSize, mCont);
                        mPredFig.copy(mFig);
                        if ((F2.minX() >= 0) && (!mFall.checkFall(F2))) {
                            mFig.rotate();
                        }
                        QTY_rotate--;
                        if((mDirection != 0) && (QTY_rotate == 0)) {
                            QTY_rotate++;
                        }
                        //mDirection = 0;
                        return;

                }
            }

        }

    }
    */



    public void updateSimple() {
        if (mDirection == 0) {
            return;
        }
        Figure F2 = new Figure(mSize, mCont);
        F2.copy(mFig);
        switch (mDirection) {
            case(1):
               F2.right();
               if ((F2.maxX() <= FieldWidth - 1) && (!mFall.checkFall(F2))) {
                   mFig.right();
               }
               //mDirection = 0;
               break;
            case(2):
               F2.left();
               if ((F2.minX() >= 0) && (!mFall.checkFall(F2))) {
                  mFig.left();
               }
               //mDirection = 0;
               break;
            case(3):
               F2.rotate();
               if ((F2.minY() >= 0) && (F2.maxX() <= FieldWidth - 1)
                        && (F2.minX() >= 0) && (!mFall.checkFall(F2))) {
                  mFig.rotate();
               }
               mDirection = 0;
               break;

        }


        return;
   }


	public void updateOneBall() {
		if (mDirection == 0) {
			return;
		}
		if (mDirection == 1) {
			OneBallFigure F2 = new OneBallFigure(mSize, mCont);
			F2.copy(mOneFig);
            F2.right();

			if ((F2.maxX() <= FieldWidth - 1) && (!mFall.checkFallOne(F2)))
				mOneFig.right();
			mDirection = 0;
			return;
		}
		if (mDirection == 2) {
            OneBallFigure F2 = new OneBallFigure(mSize, mCont);
            F2.copy(mOneFig);
			F2.left();
			if ((F2.minX() >= 0) && (!mFall.checkFallOne(F2)))
				mOneFig.left();
			mDirection = 0;
			return;
		}
		if (mDirection == 3) {
            OneBallFigure F2 = new OneBallFigure(mSize, mCont);
            F2.copy(mOneFig);
			F2.rotate();
			if ((F2.minY() >= 0) && (F2.maxX() <= FieldWidth - 1)
					&& (F2.minX() >= 0) && (!mFall.checkFallOne(F2)))
				mOneFig.rotate();
			mDirection = 0;
			return;
		}
	}



	public void updateBonus() {
		if (mDirection == 0) {
			return;
		}
        BonusFigure BF2 = new BonusFigure(mSize, mCont);
        BF2.copy(mBonFig);
        switch (mDirection) {
            case(1):
                BF2.right();
                if ((BF2.maxX() <= FieldWidth - 1) && (!mFall.checkFallBonus(BF2))) {
                    mBonFig.right();
                }
                mDirection = 0;
                break;
            case(2):
                BF2.left();
                if ((BF2.minX() >= 0) && (!mFall.checkFallBonus(BF2))) {
                    mBonFig.left();
                }
                mDirection = 0;
                break;
            case(3):
                BF2.rotate();
                if ((BF2.minY() >= 0) && (BF2.maxX() <= FieldWidth - 1)
                        && (BF2.minX() >= 0) && (!mFall.checkFallBonus(BF2))) {
                    mBonFig.rotate();
                }
                mDirection = 0;
                break;
        }

        return;

	}

	public void handleFall() {
		if (flag_mod == true) {
			if ((mFall.fallGravity(mNumRow, mFallVect))
					&& (mNumRow != FieldHeight - 1)) {
				mNumRow++;
				return;
			}
			if (mFall.check(mNumRow)) {  //get new line or lines
            	mFall.setNewGravLine(); //new line was killed
                mPoint += Constants.pointForGravityLine * mFall.getKoef() ;
	            flag_fire = true;
                flagShowPointsInfo = true;
                BallFactory.isAnimateGoal  = true;
                timeFire = System.currentTimeMillis();
				if (mSound) {
                    Music.play(mCont, R.raw.point3);
                }
				mNumLine = mFall.num;
			}
			flag_mod = false;
		}
	}

	public void SimpleFigure() {
		Figure F2;
		F2 = new Figure(mSize, mCont);
		F2.copy(mFig);
		F2.down();
		if (F2.maxY() > FieldHeight - 1) {
			mFall.modifMtrx(mFig);
			if (mFall.check(mFig.maxY())) {
				mFig.setStatus(Constants.STATUS_NEW);
				mPoint += Constants.pointUsualLine * mFall.getKoef();
				if ((mFall.getKoef() >= 4) && (mUsualSpeed < 600)) {
                    mUsualSpeed += 30;
                }
				mFall.refreshLines();
                mFall.setNewUsualLine();
				flag_fire = true;
                flagShowPointsInfo = true;
                BallFactory.isAnimateGoal = true;
                BallFactory.createAnimation(Constants.timePreview , 0.1);
				timeFire = System.currentTimeMillis();

				mNumLine = mFall.num;
			}
			mFig.copy(mNextFig);
			mNextFig = new Figure(mSize, mCont);
			mCurSpeed = mUsualSpeed;
		} else {
			if ((mFall.checkFall(mFig))
					|| ((mFall.checkFall(F2)) && (mFig.minY() <= 1)))
				mRunning = false;
			else if (mFall.checkFall(F2)) {
				mFig.setStatus(Constants.STATUS_NEW);
				if (mFig.minY() <= 0)
					mRunning = false;
				mFall.modifMtrx(mFig);
				if (mFall.check(mFig.maxY())) {
					mPoint += Constants.pointUsualLine * mFall.getKoef();
                    mFall.refreshLines();
                    mFall.setNewUsualLine();
					if ((mFall.getKoef() >= 4) && (mUsualSpeed < 600)) {
						mUsualSpeed += Constants.speedInc;
					    Log.d("Dima", "inc m Usual Speed " + mUsualSpeed);
					}

					flag_fire = true;
                    flagShowPointsInfo = true;
                    timeFire = System.currentTimeMillis();
                    BallFactory.createAnimation(Constants.timePreview , Constants.startScale );
					mNumLine = mFall.num;
					mFig.copy(mNextFig);
					mNextFig = new Figure(mSize, mCont);
					mCurSpeed = mUsualSpeed;
				} else {
					mFig.copy(mNextFig);
					mNextFig = new Figure(mSize, mCont);
					mCurSpeed = mUsualSpeed;
				}
			} else {
				mFig.setStatus(Constants.STATUS_FALLING);
				mFig.down();
			}
		}

	}

	public void BonusFigure() {
		BonusFigure BF2;
		BF2 = new BonusFigure(mSize, mCont);
		BF2.copy(mBonFig);
		BF2.down();

		if (BF2.maxY() > FieldHeight - 1) {
			mCountBonus++;
			mFall.modifMtrxBonus(mBonFig);
			if (mFall.check(mBonFig.maxY())) {
				mFig.setStatus(Constants.STATUS_NEW);
				mCountBonus = 0;
  				// flagB = true;
				mPoint += Constants.pointBonusLine * mFall.getKoef();
				if ((mFall.getKoef() >= 3) && (mUsualSpeed < 650)) {
					mUsualSpeed += Constants.speedInc;
				}
                mFall.refreshLines();
                mFall.setNewUsualLine();
				flag_fire = true;
                flagShowPointsInfo = true;
				timeFire = System.currentTimeMillis();
                BallFactory.createAnimation(5000, 0.1);
				mNumLine = mFall.num;
		 	    type_figure = 0;
                Log.d("Dima", "bonus intersect");
			}
			mCurSpeed = mUsualSpeed;
			mBonFig = new BonusFigure(mSize, mCont);

		} else {
			if ((mFall.checkFallBonus(mBonFig))
					|| ((mFall.checkFallBonus(BF2)) && (mBonFig.minY() <= 1)))
				mRunning = false;
			else if (mFall.checkFallBonus(BF2)) {
				mFig.setStatus(Constants.STATUS_NEW);
				mCountBonus++;
				if (mBonFig.minY() <= 0)
					mRunning = false;
				mFall.modifMtrxBonus(mBonFig);
				if (mFall.check(mBonFig.maxY())) {
					mCountBonus = 0;
         			mPoint += Constants.pointBonusLine * mFall.getKoef();
					if ((mFall.getKoef() >= 3) && (mUsualSpeed < 650)) {
						mUsualSpeed += Constants.speedInc;
					}
                    mFall.refreshLines();
                    mFall.setNewUsualLine();
					flag_fire = true;
					timeFire = System.currentTimeMillis();
					mNumLine = mFall.num;
					mCurSpeed = mUsualSpeed;
					type_figure = 0;
                    Log.d("Dima", "bonus intersect2");
					mBonFig = new BonusFigure(mSize, mCont);

				} else {
					mCurSpeed = mUsualSpeed;
					mBonFig = new BonusFigure(mSize, mCont);

				}
			} else {
				mFig.setStatus(Constants.STATUS_FALLING);
				mBonFig.down();
			}
		}
	}

    public void OneBallFigure() {
        OneBallFigure F2;
        F2 = new OneBallFigure(mSize, mCont);
        mOnePred = new OneBallFigure(mSize, mCont);
        mOnePred.copy(mOneFig);
        F2.copy(mOneFig);
        F2.down();
        if (F2.maxY() > FieldHeight - 1) {
            mCountOneBall++;
            mFall.modifOneBallMtrx(mOneFig);
            if (mFall.check(mOneFig.maxY())) {
				mFig.setStatus(Constants.STATUS_NEW);
                mPoint += Constants.pointUsualLine * mFall.getKoef();
                mFall.refreshLines();
                mFall.setNewUsualLine();
                flag_fire = true;
                timeFire = System.currentTimeMillis();
                mNumLine = mFall.num;
            }
            mOneFig = new OneBallFigure(mSize, mCont);
            //mFig.copy(mNextFig);
            //mNextFig = new OneBallFigure(mSize, mCont);
            mCurSpeed = mUsualSpeed;
        } else {
            if ((mFall.checkFallOne(mOneFig)) || ((mFall.checkFallOne(F2)) && (mOneFig.minY() <= 1))) {
                mRunning = false;
            }
            else if (mFall.checkFallOne(F2)) {
				mFig.setStatus(Constants.STATUS_NEW);
                if (mOneFig.minY() <= 0) {
                    mRunning = false;
                }
                mFall.modifOneBallMtrx(mOneFig);
                if (mFall.check(mOneFig.maxY())) {
                    mPoint += Constants.pointUsualLine * mFall.getKoef();
                    mFall.refreshLines();
                    mFall.setNewUsualLine();
                    flag_fire = true;
                    timeFire = System.currentTimeMillis();
                    BallFactory.createAnimation(5000, 0.1);
                    mNumLine = mFall.num;
                    mOneFig = new OneBallFigure(mSize, mCont);
                    //mFig.copy(mNextFig);
                    //mNextFig = new OneBallFigure(mSize, mCont);
                    mCurSpeed = mUsualSpeed;
                } else {
                    //mFig.copy(mNextFig);
                    //mNextFig = new OneBallFigure(mSize, mCont);
                    mOneFig = new OneBallFigure(mSize, mCont);
                    mCurSpeed = mUsualSpeed;
                }
            } else {
				mFig.setStatus(Constants.STATUS_FALLING);
                mOneFig.down();
            }
        }

    }

    public  void sendToToasts(int type) {
        Log.d("Notifs", "try to make toast");
        Message msg = new Message();
        Bundle b = new Bundle();
        b.putInt("Type notification", type);
        if(type == 0) {
            b.putInt("Level", mLevel);
        }
		if(type == 3) {
			b.putInt("Point1", mFall.getUsualLine());
			b.putInt("Point2", mFall.getGravLine()) ;
		}
        msg.setData(b);
        mHandler.sendMessage(msg);


    }

    private   void setNewLevelBounds() {
        mPrevLevelBound = mNewLevelBounds;
        mNewLevelBounds = mLevel  * Constants.newLevelKoef + Math.abs(random.nextInt()) % Constants.maxAddPart;
    }

    private void setNewSpeedUpBound() {
        mNewSpeedBound = (mNewBoundsBonus + mPrevLevelBound) / 2;
    }

    public void setNewBonusBounds() {
        int avgPoint = (Constants.pointBonusLine + Constants.pointForGravityLine + Constants.pointUsualLine) / 3;
        mNewBoundsBonus = mNewLevelBounds - (Math.abs(random.nextInt()) % 3) * avgPoint;
    }



	public void run() {
		time = System.currentTimeMillis();
        //set new level



		while (mRunning) {
			if (mPause)
				continue;

            mCurGoal = BallFactory.getGoal();
			if(mPoint > mNewSpeedBound) {
                if(mUsualSpeed - Constants.speedInc >= Constants.speedMin) {
                    mUsualSpeed -= Constants.speedInc;
                }
                mCurSpeed = mUsualSpeed;
				mNewSpeedBound = Constants.infinity;
			}

			if((flagShowPointsInfo == true) && (mUsualSpeed <= Constants.speedCouldUp) &&
              ((mFall.getUsualLine() >= Constants.PointU) || (mFall.getGravLine() >= Constants.PointN))) {
                if(mUsualSpeed + Constants.speedInc <= Constants.speedMax) {
                    mUsualSpeed += Constants.speedInc;
                }

                mCurSpeed = mUsualSpeed;
                sendToToasts(3);
                try {
                    sleep(Constants.SuperFigureDuration);
                } catch (InterruptedException e) {
                }

                flagShowPointsInfo = false;
            }

			if (mPoint >= mNewLevelBounds) {
                mLevel += 1;
                setNewLevelBounds();
                setNewBonusBounds();
                setNewSpeedUpBound();
                BallFactory.setNewGoal();
			}
            Log.d("Dima", mFall.getGravLine() + " " + mFall.getKoef() + "POINTS ");
            //speed up
            Log.d("Dima", mNewSpeedBound + " " + mCurSpeed +  "speed bounds");




			if (mInitialized) {
                //launch one ball
                Log.d("Dima max Rows", mFall.getQtyFilledRows() + "");
				if(mFall.getQtyFilledRows() > FieldHeight / 2) {
                    if((mPoint >= 100 * mOneMult) && (type_figure != 2) && (mFig.getStatus() == Constants.STATUS_NEW)) {
                        if(timeProblem == 0) {
                            timeProblem =  System.currentTimeMillis(); //First get in hard position
                        } else {
                            if(System.currentTimeMillis() - timeProblem >= Constants.delayHard) { //Too long in hard pos

								sendToToasts(2);

								try {
									sleep(Constants.SuperFigureDuration);
								} catch (InterruptedException e) {
								}

								type_figure = 2; //set one Figure
								mCurSpeed = Constants.speedOne;
                                mCurMaxOne = Math.abs(random.nextInt()) % Constants.MaxOne;
                                timeProblem = 0;
                            }
                        }
                    }
				}
                if ((mCountOneBall == mCurMaxOne) && (type_figure == 2)) {
                    mOneMult += 3;
                    type_figure = 0;
                    mCurSpeed = mUsualSpeed;
                    mCountOneBall = 0;
                }
                //launch bonus
                Log.d("Dima", mNewBoundsBonus + "Bonus bound");
                Log.d("Doma", mCountBonus + " Count bonus");

                if ((mPoint >=  mNewBoundsBonus) && (mCountBonus == 0)) {

					mCountBonus = 1;
					if (type_figure != 1) {

                        sendToToasts(1);

						try {
							sleep(Constants.SuperFigureDuration);
						} catch (InterruptedException e) {
						}

						type_figure = 1; //set Bonus Figure
                        mCurMaxBonus =  Math.abs(random.nextInt()) % Constants.MaxBonus + 2;
                        Log.d("Dima", mCurMaxBonus + "QTY bonus");
					}
				}
				if ((mCountBonus == mCurMaxBonus) && (type_figure == 1)) {
                    Log.d("Dima", "approach bonus");
          			type_figure = 0;
                    mNewBoundsBonus = Constants.infinity;
					mCountBonus = 0;
				}
				draw(drawObjects);
                if (timeLastMove == 0) { //just press the button or not press ever
                    updateStates();
                    if(mDirection > 0) {
                        timeLastMove = System.currentTimeMillis();
                    }
                } else {
                    if (System.currentTimeMillis() - timeLastMove > delayMove2) {
                        updateStates();
                        timeLastMove = System.currentTimeMillis();
                    }

                }

				if (mRunning == false)
					break;

				if (flag_mod) {
					try {
						sleep(40);
					} catch (InterruptedException e) {
					}

				}
            if(mLevel > mPrevLevel) {
                    //sendToToasts(0);


                    sendToToasts(0);
                    try {
                        sleep(Constants.AchieveDuration);
                    } catch (InterruptedException e) {
                    }

                }
                mPrevLevel = mLevel;
			}

		}
		draw(drawGameover);
	}



    @SuppressLint("NewApi")
    public void initPositions(int screenHeight, int screenWidth) {
        synchronized (mSurfaceHolder) {
            long time_start = System.currentTimeMillis();
            Log.d("Dima", "Flag new" + mFall.flag_new);
            mInitialized = false;
            mWidth = screenWidth;
            mHeight = screenHeight;
			mExit = 1;
            float DPI = mCont.getResources().getDisplayMetrics().densityDpi;
            float px = BUTT_SIZE_MM * DPI;
            float pause_px = PAUSE_SIZE_MM * DPI;
            pause_px = pause_px  / (float) 25.4;


            px = px / (float) 25.4;
            // float px = (BUTT_SIZE_MM * DPI) / 25.4;
            // float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM,
            // BUTT_SIZE_MM, mCont.getResources().getDisplayMetrics());
            int mButtMaxSize = Math.round(px);

            if (mWidth / 6 < mButtMaxSize) {
                mButtSize = mWidth / 6;
            } else {
                mButtSize = mButtMaxSize;
            }
            //mButtSize = 0;


			int k = Integer.valueOf(mSettings.getString("game_mode", "2"));
			if (k == 2) {
                flagClass = false;
            }

			//SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
			//if doesn't exit
            mCurSpeed = mSettings.getInt("Speed", 350);
            Log.d("Exit", mSettings.getInt("Exit", 1) + " Exit ");
			if(mSettings.getInt("Exit", 1) == 1) {
				mCurSpeed = mSettings.getInt("Speed", 350);
				mUsualSpeed = mCurSpeed;
				mLevel = mSettings.getInt("Level", 1);
				mPoint = mSettings.getInt("Score", 0);
				mCountBonus = mSettings.getInt("Bonus", 0); //Increment for Bonus Figure
				mCountOneBall = mSettings.getInt("OneBall", 0);
				mNewLevelBounds = mSettings.getInt("LevelBounds", mNewLevelBounds);
                mNewBoundsBonus = mSettings.getInt("LevelBonus", mNewBoundsBonus);
                BallFactory.setGoal(mSettings.getInt("CurGoal", 0));
                SharedPreferences.Editor editor = mSettings .edit();
                editor.putInt("Exit", 0);
                editor.commit();
              	//mPoint = Integer.valueOf(settings.getString("Score", "0"));
			} else {

				mCurSpeed = mUsualSpeed;
			}
            setNewLevelBounds();
            setNewBonusBounds();
            Log.d("rh4h4", mNewLevelBounds + " ");
			mVibration = mSettings.getBoolean("pref_vibration", true);
			mSound = mSettings.getBoolean("pref_sound", true);



			delayMove2 = mSettings.getInt("sensitivity", delayMove2);
            mTypeControl = Integer// Number of old theme
                    .valueOf(mSettings.getString("control", "0"));
            if(mTypeControl == 1) { //if touchscreen
                mButtSize = 0;
            }
            mSize = (mHeight - mButtSize) / 22;
            FieldWidth = 10;
            FieldHeight = 22;
            mSizeFreeSpace = (mWidth - FieldWidth * mSize) / 2;
            if(Math.round(pause_px) < mSizeFreeSpace) {
                pause_size = Math.round(pause_px);
            } else {
                pause_size = mSizeFreeSpace;
            }

            mFallVect = new int[FieldWidth];
            if (flag_first == true) { //start first time
                mBonFig = new BonusFigure(mSize, mCont);
                mOneFig = new OneBallFigure(mSize, mCont);
                mFig = new Figure(mSize, mCont);
                mNextFig = new Figure(mSize, mCont);
                mFall.setParams(FieldWidth, FieldHeight, mSize, mCont);
                flag_first = false;
            } else {  //this is not the first start
                mFall.set(mSize);
                mFig.set(mSize);
                mNextFig.set(mSize);
                mBonFig.set(mSize);
                mOneFig.set(mSize);
            }


            mLeftButtBound = (mWidth - mButtSize * 6) / 2;
            // mFactory = new BallFactory(mCont, mSize);
            mTextSize = getTextHeight("Speed", mPaint, mSizeFreeSpace);
            mBounds = new Rect();

			BallFactory.mImageRight.setBounds(mLeftButtBound + mButtSize * 3, mHeight
                    - mButtSize, mLeftButtBound + mButtSize * 4, mHeight);
			BallFactory.mImageRightPress.setBounds(mLeftButtBound + mButtSize * 3, mHeight
                    - mButtSize, mLeftButtBound + mButtSize * 4, mHeight);
			BallFactory.mImageLeft.setBounds(mLeftButtBound, mHeight - mButtSize,
                    mLeftButtBound + mButtSize, mHeight);
			BallFactory.mImageLeftPress.setBounds(mLeftButtBound, mHeight - mButtSize,
                    mLeftButtBound + mButtSize, mHeight);
			BallFactory.mImageDown.setBounds(mLeftButtBound + mButtSize * 5, mHeight
                    - mButtSize, mLeftButtBound + mButtSize * 6, mHeight);
			BallFactory.mImageDownPress.setBounds(mLeftButtBound + mButtSize * 5, mHeight
                    - mButtSize, mLeftButtBound + mButtSize * 6, mHeight);
			BallFactory.mImageRotate.setBounds(mLeftButtBound + mButtSize * 3 / 2, mHeight
                    - mButtSize, mLeftButtBound + mButtSize * 5 / 2, mHeight);
			BallFactory.mImageRotatePress.setBounds(mLeftButtBound + mButtSize * 3 / 2,
                    mHeight - mButtSize, mLeftButtBound + mButtSize * 5 / 2,
                    mHeight);

			BallFactory.mImagePauseStp.setBounds(mSizeFreeSpace / 2 - pause_size / 2, 0, mSizeFreeSpace / 2 + pause_size / 2, pause_size);
			BallFactory.mImagePausePl.setBounds(mSizeFreeSpace / 2 - pause_size / 2, 0, mSizeFreeSpace / 2 + pause_size / 2, pause_size);
            BallFactory.mImageSettings.setBounds(mSizeFreeSpace / 2 - pause_size / 2, pause_size + pause_size / 2, mSizeFreeSpace / 2 + pause_size / 2, 2 * pause_size + pause_size / 2);
            BallFactory.mImageMusicPlay.setBounds(mSizeFreeSpace / 2 - pause_size / 2, 3 * pause_size, mSizeFreeSpace / 2 + pause_size / 2, 4 * pause_size);
            BallFactory.mImageMusicStop.setBounds(mSizeFreeSpace / 2 - pause_size / 2, 3 * pause_size, mSizeFreeSpace / 2 + pause_size / 2, 4 * pause_size);
			BallFactory.mImageWall = BallFactory.scaleBitmap(BallFactory.mImageWall, null, R.drawable.grass,  mWidth, mHeight);
            //BallFactory.setNewBack(FieldWidth * mSize, FieldHeight * mSize);
			BallFactory.mImageBack = BallFactory.scaleBitmap(BallFactory.mImageBack, null, R.drawable.field_tele, FieldWidth * mSize, FieldHeight * mSize);
            //FrameSize 0.7x, 0.2y.  280X176
			BallFactory.setNewBackFrame((int)Math.round(FieldWidth * mSize * 0.7), (int)Math.round(FieldHeight * mSize * 0.2));
			//BallFactory.mImagePhone3 = BallFactory.scaleBitmap(R.drawable.ground4, FieldWidth * mSize, FieldHeight * mSize);
            //BallFactory.mImagePhone = BallFactory.decodeSampledBitmapFromResource(R.drawable.ground,  FieldWidth * mSize, FieldHeight * mSize);
            //BallFactory.mImagePhone2 = BallFactory.decodeSampledBitmapFromResource(R.drawable.ground3,  FieldWidth * mSize, FieldHeight * mSize);
            //BallFactory.mImagePhone3 = BallFactory.decodeSampledBitmapFromResource(R.drawable.ground3, FieldWidth * mSize, FieldHeight * mSize);
            //mImageWall = Bitmap.createScaledBitmap(BallFactory.mImageWall,
            //        mWidth, mHeight, true);

            BallFactory.blackImage = BallFactory.scaleBitmap(BallFactory.blackImage, null,  R.drawable.ball_black, mSize, mSize);
            BallFactory.blueImage = BallFactory.scaleBitmap(BallFactory.blueImage, null,R.drawable.ball_blue, mSize, mSize);
            BallFactory.redImage = BallFactory.scaleBitmap(BallFactory.redImage, null,R.drawable.ball_red, mSize, mSize);
            BallFactory.orangeImage = BallFactory.scaleBitmap(BallFactory.orangeImage, null,R.drawable.ball_orange, mSize, mSize);
            BallFactory.yellowImage = BallFactory.scaleBitmap(BallFactory.yellowImage, null,R.drawable.ball_yellow, mSize, mSize);
            BallFactory.purpleImage = BallFactory.scaleBitmap(BallFactory.purpleImage, null,R.drawable.ball_purple, mSize, mSize);
		    mRectPoint.set(FieldWidth * mSize + (mWidth - FieldWidth * mSize)
                    / 2, 0, mWidth, FieldHeight * mSize);
            mField.set(0, 0, FieldWidth * mSize, FieldHeight * mSize);
            mInitialized = true;
            long time_end = System.currentTimeMillis();
            long t = time_end - time_start;

        }

    }


	public void settingShanged() {
        mVibration = mSettings.getBoolean("pref_vibration", true);
        mSound = mSettings.getBoolean("pref_sound", true);
        initPositions( mHeight, mWidth);



	}



    public void unpause() {
       mPause = false;
       draw(drawObjects);

    }


	public void pause() {
	   mPause = true;
       SharedPreferences.Editor editor = mSettings .edit();
	   editor.putInt("Speed", mUsualSpeed);
	   editor.putInt("Level", mLevel);
       editor.putInt("Score", mPoint);
       editor.putInt("Bonus", mCountBonus);
       editor.putInt("OneBall", mCountOneBall);
	   editor.putInt("LevelBounds", mNewLevelBounds);
       editor.putInt("LevelBonus", mNewBoundsBonus);
       editor.putInt("CurGoal", mCurGoal);
       editor.putInt("needRefresh", mRefreshFig );
       editor.putInt("needRefreshFalling", mRefreshFalling);

       mFig.saveFigure();
       mFall.saveFalling();
	   Log.d("Dima", "Pause");
       editor.putInt("Exit", mExit);
       editor.commit();

	}

	public void upper() {
		flag_r = false;
		flag_l = false;
        mDirection = 0;
	}

	public void left() {
        timeLeft = 0;
        flag_l = true;
        mHandlerLeft.sendMessageDelayed(mHandlerLeft.obtainMessage(0),
				Constants.delayMove);
        mDirection = 2;


    }


    public void right() {
        timeRight = 0;
        flag_r = true;
        mHandlerRight.sendMessageDelayed(mHandlerRight.obtainMessage(0),
                Constants.delayMove);
        mDirection = 1;

    }
	public void rotate() {

    	flag_rot = true;
		mDirection = 3;
	}



	public void speedDown() {
		flag_down = true;
		mCurSpeed = mSpeedDown;
	}
}
