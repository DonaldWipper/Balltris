package org.project2;

//import org.table.TableRecords;

import org.online_table.TableWeb;
import org.project3.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

public class StartScreen2 extends  Activity implements OnClickListener {
	/** Called when the activity is first created. */

	public static final String PREFS_NAME = "MyPrefsFile";
	private final String name = "null";
	private int score = 0;
    private int inc = 0;
    SharedPreferences prefs;
    private WebView About = null;
    StringBuilder sb = new StringBuilder();
    StringBuilder sbjs = new StringBuilder();
    Music music;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// Bundle extras = getIntent().getExtras();
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.about);

        openCoverWindow();
        // making it full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        About = (WebView) findViewById(R.id.about);

		WebViewUtils.loadTemplateFile(this, sb, "main_menu.html");
        WebViewUtils.loadTemplateFile(this, sbjs, "index_menu.js");


	}

    public void onResume() {
        super.onResume();
        String a = "var links = ";

        About.clearView();
        if(prefs.getInt("Exit", 0) == 1) {
            a += "[{label: 'continue', bg: '#c0392b'}," + //0
                    "{label: 'restart', bg: '#c0392b'},";    //1
            inc = 0;
        } else {
            a += "[{label: 'start', bg: '#c0392b'},";     //2
            inc = 1;
        }
        String labels = a +
                "{label: 'high scores', bg: '#16a085'}, " +            //2
                "{label: 'settings', bg: '#8e44ad'}," +                //3
                //"{label: 'achievements', bg: '#8e44ad'}," +            //4
                "{label: 'about', bg: '#f39c12'}," +                   //4
                "{label: 'help', bg: '#27ae60'}," +                    //5
                //"{label: 'cup conf 2017', bg: '#f39c12'}," +           //7
                "{label: 'exit', bg: '#2980b9'}]; ";                   //6

        String summary = sb.toString() + labels + sbjs.toString();
        About.setWebViewClient(new WebViewClient());
        WebViewUtils.loadData(About, summary);
		Log.d("Dima", "summatu " + summary);
        About.getSettings().setJavaScriptEnabled(true);
        About.getSettings().setDomStorageEnabled(true);
        About.addJavascriptInterface(new WebAppInterface(this), "Android");
        About.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
    }


    private  void openCoverWindow() {
        Intent intent = new Intent();
        intent.setClass(this, CoverScreen.class);
        this.startActivity(intent);
    }


    //Class to be injected in Web page
	public class WebAppInterface {
		Context mContext;

		/** Instantiate the interface and set the context */
		WebAppInterface(Context c) {
			mContext = c;
		}

		/**
		 * Show Toast Message
		 * @param toast
		 */
		public void showToast(String toast) {
			Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
		}

		/**
		 * Show Dialog
		 * @param
		 */
		public void showTouchX(int X) {
			showToast("" + X);
		}

		public void startItem(int i) {
            if(i == 0) {
                Intent intent = new Intent();
                intent.setClass(mContext, main.class);
                startActivity(intent);
                return;
            }
            if((inc == 0) && (i == 1)) { //restart game
                int mUsualSpeed  = 300;
                int mLevel = 1;
                int mPoint = 0;
                int mCountBonus = 1;
                int mCountOneBall = 1;
                int mRefreshFig = 0;
                int mRefreshFalling = 0;
                BallFactory.refreshImages();
                SharedPreferences.Editor editor = prefs .edit();
                editor.putInt("Speed", mUsualSpeed);
                editor.putInt("Level", mLevel);
                editor.putInt("Score", mPoint);
                editor.putInt("Bonus", mCountBonus);
                editor.putInt("OneBall", mCountOneBall);
                editor.putInt("CurGoal", 0);
                editor.putInt("needRefresh", mRefreshFig );
                editor.putInt("needRefreshFalling", mRefreshFalling);
                editor.commit();
                Intent intent = new Intent();
                intent.setClass(mContext, main.class);
                startActivity(intent);
                return;


            }

			switch (inc + i) {
				case 2: {
					Intent intent = new Intent();
					intent.setClass(mContext, TableWeb.class);
					intent.putExtra(TableWeb.Name, name);
					intent.putExtra(TableWeb.Score, score);
					intent.putExtra(TableWeb.Mode, "0");
					startActivity(intent);
					break;
				}

				case 3: {
					Intent intent = new Intent();
					intent.setClass(mContext, SettingsScreen.class);
					startActivity(intent);
					break;
				}

				case 4: {
					Intent intent = new Intent();
					intent.setClass(mContext, About.class);
					startActivity(intent);
					break;
				}
				case 5: {
					Intent intent = new Intent();
					intent.setClass(mContext, Help.class);
					startActivity(intent);
					break;
				}
				case 6:
					finish();
					break;

				default:
					break;
			}
		}
	}


	public void onClick(View v) {
		switch (v.getId()) {

			default:
				break;
		}
	}

}

