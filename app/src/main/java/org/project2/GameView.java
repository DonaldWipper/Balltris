package org.project2;

import org.project3.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {
	private SurfaceHolder mSurfaceHolder;
	private Context cont;
	private Handler mHand ;
	private static GameManager mGameManager;
    public static Falling mFalling;
	private ProgressDialog mPrgDialog;
	private float mXFirst = 0;
    private float mYFirst = 0;
    private float mXNext = 0;
    private float mYNext = 0;


	public GameView(Context context, Handler handler, ProgressDialog progressDialog) {
        super(context);
        mPrgDialog = progressDialog;
		((Activity) context).setContentView(R.layout.load);
		cont = context;
		mHand = handler;
		mSurfaceHolder = getHolder();
		mSurfaceHolder.addCallback(this);
		//mGameManager = new GameManager(mSurfaceHolder, context, handler);
		//mGameManager.mFactory =  new BallFactory (context);
		mFalling = new Falling();
		mFalling.flag_new = 0;
		setFocusable(true);
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

		mGameManager.initPositions(height, width);

	}

	public void ActionUp() {
        GameManager.mFlagPressFig = 0;
        GameManager.timeLastMove = 0;
        //if movement was a little
        if ((Math.abs(mXNext - mXFirst) < 4) && (Math.abs(mYNext - mYFirst) < 4) && (mXNext != 0)) {
            mGameManager.rotate();
            return;
        } else
            mGameManager.upper();
    }


	public void surfaceCreated(SurfaceHolder holder) {
		mGameManager = new GameManager(mSurfaceHolder, cont, mHand, mFalling);
		mGameManager.mFactory =  new BallFactory (cont);
		mGameManager.setRunning(true);
		mGameManager.start();
        mPrgDialog.dismiss();

		// mGameManager2.start();
	}

	public static GameManager getThread() {
		return mGameManager;
	}
	

	

	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		mGameManager.setRunning(false);
		GameManager.mFactory.makeFree();
		while (retry) {
			try {
				mGameManager.join();
				retry = false;
			} catch (InterruptedException e) {
			}
		}
	}





    private  void openSettingsWindow() {
        Intent intent = new Intent();
        intent.setClass(cont, SettingsScreen.class);
        cont.startActivity(intent);
    }
	
	

	public boolean onTouchEvent(MotionEvent event) {
		int action = event.getAction();
		int pause_radius = GameManager.pause_size  / 2;
		//touchscreen will work everywhere exept of button's field
		int fieldHeight = GameManager.FieldHeight * GameManager.mSize;
        int cntrButtX = GameManager.mSizeFreeSpace / 2;
        int cntrButtPauseY = pause_radius;
        int cntrSttngsButtY = 2 * GameManager.pause_size;


		switch (action) {
		case (MotionEvent.ACTION_DOWN):
            Log.d("Dima aa", mXNext + " " + mYNext);
			GameManager.timeBtnPress = System.currentTimeMillis();
			if ((event.getY() >= getHeight() - GameManager.mButtSize)
					&& (event.getY() <= getHeight())) {
				if ((event.getX() >= GameManager.mLeftButtBound)
						&& (event.getX() <= GameManager.mLeftButtBound
								+ GameManager.mButtSize)) {
                    if(mGameManager.mDirection == 0) {
                        mGameManager.left();
                    }

				}
				if ((event.getX() >= GameManager.mLeftButtBound
						+ GameManager.mButtSize * 3 / 2)
						&& (event.getX() <= GameManager.mLeftButtBound
								+ GameManager.mButtSize * 5 / 2)) {
					mGameManager.rotate();
				}

				if ((event.getX() >= GameManager.mLeftButtBound
						+ GameManager.mButtSize * 3)
						&& (event.getX() <= GameManager.mLeftButtBound
								+ GameManager.mButtSize * 4)) {
                    if(mGameManager.mDirection == 0) {
                        mGameManager.right();
                    }

				}
				if ((event.getX() >= GameManager.mLeftButtBound
						+ GameManager.mButtSize * 5)
						&& (event.getX() <= GameManager.mLeftButtBound
								+ GameManager.mButtSize * 6)) {
                    if(mGameManager.mDirection == 0) {
                        mGameManager.speedDown();
                    }
				}
			}
			if ((event.getX() - cntrButtX) * (event.getX() - cntrButtX) + (event.getY() -  cntrSttngsButtY)
					* (event.getY() - cntrSttngsButtY) <= pause_radius * pause_radius) {
                openSettingsWindow();
			}



            if ((event.getX() - cntrButtX) * (event.getX()  - cntrButtX) + (event.getY() -  cntrButtPauseY )
                    * (event.getY() - cntrButtPauseY) <= pause_radius * pause_radius) {
                if(mGameManager.mPause == true) {
                    mGameManager.unpause();
                    break; //don't allow to use touchscreen further
                } else {
                    mGameManager.pause();
                    break;
                }
            }

            if (event.getY() < fieldHeight) {
                if(GameManager.mTypeControl == 2) {
                    break;
                } //if no touchscreen control
                GameManager.mFlagPressFig = 1;
                mXFirst = event.getX();
                mYFirst = event.getY();
                mXNext = mXFirst;
                mYNext = mYFirst;


            }
				
			break;
         case (MotionEvent.ACTION_MOVE):
             if (mXFirst != 0) {
                 mXNext = event.getX();
                 mYFirst = event.getY();
                 Log.d("Dima aa", mXNext + " " + mYNext);

                 //if movement by OY was bigger then by OX
                 if ((mYFirst - mYNext > 10)  && ((mYFirst - mYNext) > (mXNext - mXFirst))) {
                         if(mGameManager.mDirection == 0) {
                             mGameManager.speedDown();
                         }
                 } else {
                      if (mXNext > mXFirst) {
                          if(mGameManager.mDirection == 0) {
                              mGameManager.right();
                          }
                             mXFirst = mXNext = 0;
                             break;
                         } else {
                             if (mXNext < mXFirst) {
                                 if(mGameManager.mDirection == 0) {
                                     mGameManager.left();
                                 }
                                 mXFirst = mXNext = 0;

                                 break;
                             }
                         }
                     }

             }


         break;

		/*
		 * case (MotionEvent.ACTION_MOVE): if ((event.getY() >= getHeight() -
		 * GameManager.mButtSize) && (event.getY() <= getHeight()) &&
		 * (event.getX() >= GameManager.mLeftButtBound + GameManager.mButtSize *
		 * 3) && (event.getX() <= GameManager.mLeftButtBound +
		 * GameManager.mButtSize * 4)) { mGameManager.right(); break; } if
		 * ((event.getY() >= getHeight() - GameManager.mButtSize) &&
		 * (event.getY() <= getHeight()) && (event.getX() >=
		 * GameManager.mLeftButtBound) && (event.getX() <=
		 * GameManager.mLeftButtBound + GameManager.mButtSize)) {
		 * mGameManager.left(); break; }
		 */
		case (MotionEvent.ACTION_UP):
            ActionUp();
            break;

		}

		return true;
	}

}
