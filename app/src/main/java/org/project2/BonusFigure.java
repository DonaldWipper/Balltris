package org.project2;

import java.util.Random;

import org.project3.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;

public class BonusFigure {
	public int F[];
	public int S[];
	public int T[];
	public int Fr[];
	public int F2[];
	public int S2[];
	public static Random random = new Random();
	public static Random random2 = new Random();
	public int CubeSize;
	public int Color;
	private Context mCont;

	public void renderX() {
		F[0] = 5;
		F[1] = 0;
		S[0] = 6;
		S[1] = 1;
		T[0] = 5;
		T[1] = 1;
		Fr[0] = 4;
		Fr[1] = 1;
		F2[0] = 5;
		F2[1] = 2;
		S2[0] = 5;
		S2[1] = 2;

	}

	public void renderS() {
		F[0] = 5;
		F[1] = 0;
		S[0] = 6;
		S[1] = 0;
		T[0] = 5;
		T[1] = 1;
		Fr[0] = 5;
		Fr[1] = 2;
		F2[0] = 5;
		F2[1] = 3;
		S2[0] = 4;
		S2[1] = 3;

	}

	public void renderG() {
		F[0] = 5;
		F[1] = 0;
		S[0] = 5;
		S[1] = 1;
		T[0] = 5;
		T[1] = 2;
		Fr[0] = 4;
		Fr[1] = 2;
		F2[0] = 3;
		F2[1] = 2;
		S2[0] = 3;
		S2[1] = 2;
	}

	public void renderU() {
		F[0] = 5;
		F[1] = 0;
		T[0] = 5;
		T[1] = 1;
		Fr[0] = 5;
		Fr[1] = 2;
		S[0] = 6;
		S[1] = 2;
		F2[0] = 5;
		F2[1] = 3;
		S2[0] = 4;
		S2[1] = 2;
	}

	public void renderO() {
		F[0] = 6;
		F[1] = 0;
		S[0] = 5;
		S[1] = 0;
		T[0] = 5;
		T[1] = 1;
		Fr[0] = 6;
		Fr[1] = 1;
	}

	public void renderI() {
		F[0] = 5;
		F[1] = 0;
		T[0] = 5;
		T[1] = 1;
		S[0] = 5;
		S[1] = 2;
		Fr[0] = 5;
		Fr[1] = 3;
	}

	public void rotate() {
		int x1 = T[0] - F[0];
		int y1 = T[1] - F[1];
		int x2 = T[0] - S[0];
		int y2 = T[1] - S[1];
		int x3 = T[0] - Fr[0];
		int y3 = T[1] - Fr[1];
		int z1 = T[0], z2 = T[1];
		int _x1 = T[0] - F2[0];
		int _y1 = T[1] - F2[1];
		int _x2 = T[0] - S2[0];
		int _y2 = T[1] - S2[1];
		int _z1 = T[0], _z2 = T[1];
		F[0] = y1 + z1;
		F[1] = -x1 + z2;
		S[0] = y2 + z1;
		S[1] = -x2 + z2;
		Fr[0] = y3 + z1;
		Fr[1] = -x3 + z2;
		F2[0] = _y1 + _z1;
		F2[1] = -_x1 + _z2;
		S2[0] = _y2 + _z1;
		S2[1] = -_x2 + _z2;
	}

	public void copy(BonusFigure _F2) {
		F[0] = _F2.F[0];
		F[1] = _F2.F[1];
		S[0] = _F2.S[0];
		S[1] = _F2.S[1];
		T[0] = _F2.T[0];
		T[1] = _F2.T[1];
		Fr[0] = _F2.Fr[0];
		Fr[1] = _F2.Fr[1];
		F2[0] = _F2.F2[0];
		F2[1] = _F2.F2[1];
		S2[0] = _F2.S2[0];
		S2[1] = _F2.S2[1];
		Color = _F2.Color;
		CubeSize = _F2.CubeSize;
	}

	public void set(int s) {
		CubeSize = s;
	}

	public BonusFigure(int Size, Context context) {
		F = new int[2];
		S = new int[2];
		T = new int[2];
		Fr = new int[2];
		F2 = new int[2];
		S2 = new int[2];
		mCont = context;
		int i = Math.abs(random.nextInt()) % 4;
		Color = Math.abs(random2.nextInt()) % 5;
		CubeSize = Size;
		switch (i) {
		case (0):
			renderX();
			break;
		case (1):
			renderS();
			break;
		case (2):
			renderG();
			break;
		case (3):
			renderU();
			break;
		// case(4): renderO(); break;
		// case(5): renderU(); break;
		// case(6): renderL(); break;
		}

	}

	public void paint(Canvas canvas, Paint mPaint) {
		// i = Math.abs( random.nextInt() ) % 200 + 1;
		// g.setColor(i);
		Bitmap mImage1;
		SharedPreferences prefs;
		mImage1 = BallFactory.blackImage;
		prefs = PreferenceManager.getDefaultSharedPreferences(mCont);
		int numTheme = Integer// Number of old theme
				.valueOf(prefs.getString("ballThemes", "6"));
		if (numTheme == 6) {
			switch (Color) {
			case (0):
				mImage1 = BallFactory.blackImage;
				break;
			case (1):
				mImage1 = BallFactory.purpleImage;
				break;
			case (2):
				mImage1 = BallFactory.orangeImage;
				break;
			case (3):
				mImage1 = BallFactory.redImage;
				break;
			case (4):
				mImage1 = BallFactory.yellowImage;
				break;
			case (5):
				mImage1 = BallFactory.blueImage;
				break;
			}
		} else {
			switch (numTheme) {
			case (0):
				mImage1 = BallFactory.blackImage;
				break;
			case (1):
				mImage1 = BallFactory.blueImage;
				break;
			case (2):
				mImage1 = BallFactory.orangeImage;
				break;
			case (3):
				mImage1 = BallFactory.redImage;
				break;
			case (4):
				mImage1 = BallFactory.yellowImage;
				break;
			case (5):
				mImage1 = BallFactory.purpleImage;
				break;
			}
		}
		canvas.drawBitmap(mImage1, F[0] * CubeSize, F[1] * CubeSize, null);
		canvas.drawBitmap(mImage1, S[0] * CubeSize, S[1] * CubeSize, null);
		canvas.drawBitmap(mImage1, T[0] * CubeSize, T[1] * CubeSize, null);
		canvas.drawBitmap(mImage1, Fr[0] * CubeSize, Fr[1] * CubeSize, null);
		canvas.drawBitmap(mImage1, F2[0] * CubeSize, F2[1] * CubeSize, null);
		canvas.drawBitmap(mImage1, S2[0] * CubeSize, S2[1] * CubeSize, null);

	}

	public void paint_mini(Canvas canvas, Paint mPaint, Context context) {
		// i = Math.abs( random.nextInt() ) % 200 + 1;
		// g.setColor(i);
		int Size = 15;
		int ShiftX = 150;
		int ShiftY = 100;
		Drawable mImage1;
		Resources res = context.getResources();
		mImage1 = res.getDrawable(R.drawable.ball_black);
		switch (Color) {
		case (0):
			mImage1 = res.getDrawable(R.drawable.ball_blue);
			break;
		case (1):
			mImage1 = res.getDrawable(R.drawable.ball_purple);
			break;
		case (2):
			mImage1 = res.getDrawable(R.drawable.ball_orange);
			break;
		case (3):
			mImage1 = res.getDrawable(R.drawable.ball_red);
			break;
		case (4):
			mImage1 = res.getDrawable(R.drawable.ball_yellow);
			break;
		case (5):
			mImage1 = res.getDrawable(R.drawable.ball_black);
			break;
		}

		mImage1.setBounds(ShiftX + F[0] * Size, ShiftY + F[1] * Size, ShiftX
				+ F[0] * Size + Size, ShiftY + F[1] * Size + Size);
		mImage1.draw(canvas);
		mImage1.setBounds(ShiftX + S[0] * Size, ShiftY + S[1] * Size, ShiftX
				+ S[0] * Size + Size, ShiftY + S[1] * Size + Size);
		mImage1.draw(canvas);
		mImage1.setBounds(ShiftX + T[0] * Size, ShiftY + T[1] * Size, ShiftX
				+ T[0] * Size + Size, ShiftY + T[1] * Size + Size);
		mImage1.draw(canvas);
		mImage1.setBounds(ShiftX + Fr[0] * Size, ShiftY + Fr[1] * Size, ShiftX
				+ Fr[0] * Size + Size, ShiftY + Fr[1] * Size + Size);
		mImage1.draw(canvas);
	}

	public void down() {
		F[1]++;
		S[1]++;
		T[1]++;
		Fr[1]++;
		F2[1]++;
		S2[1]++;
	}

	public void up() {
		F[1]--;
		S[1]--;
		T[1]--;
		Fr[1]--;
		F2[1]--;
		S2[1]--;
	}

	public void right() {
		F[0]++;
		S[0]++;
		T[0]++;
		Fr[0]++;
		F2[0]++;
		S2[0]++;
	}

	public void left() {
		F[0]--;
		S[0]--;
		T[0]--;
		Fr[0]--;
		F2[0]--;
		S2[0]--;
	}

	public int maxY() {
		int max = F[1];
		if (S[1] > max) {
			max = S[1];
		}
		if (T[1] > max) {
			max = T[1];
		}
		if (Fr[1] > max) {
			max = Fr[1];
		}
		if (S2[1] > max) {
			max = S2[1];
		}

		if (F2[1] > max) {
			max = F2[1];
		}
		return max;
	}

	public int minY() {
		int min = F[1];
		if (S[1] < min) {
			min = S[1];
		}
		if (T[1] < min) {
			min = T[1];
		}
		if (Fr[1] < min) {
			min = Fr[1];
		}
		if (S2[1] < min) {
			min = S2[1];
		}

		if (F2[1] < min) {
			min = F2[1];
		}
		return min;
	}

	public int maxX() {
		int max = F[0];
		if (S[0] > max) {
			max = S[0];
		}
		if (T[0] > max) {
			max = T[0];
		}
		if (Fr[0] > max) {
			max = Fr[0];
		}
		if (S2[0] > max) {
			max = S2[0];
		}

		if (F2[0] > max) {
			max = F2[0];
		}
		return max;
	}

	public int minX() {
		int min = F[0];
		if (S[0] < min) {
			min = S[0];
		}
		if (T[0] < min) {
			min = T[0];
		}
		if (Fr[0] < min) {
			min = Fr[0];
		}
		if (S2[0] < min) {
			min = S2[0];
		}

		if (F2[0] < min) {
			min = F2[0];
		}
		return min;
	}
}
