package org.project2;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import android.util.Log;

public class Falling {
	static public int [][] Mtrx;
	static public int [][] Color;
	public int[][] MtrxS;
	public int [][] ColorS;
	private int N = 0;
	private int M = 0;
	
	static public int flag_new = 1;
    private int Koef = 0;  //first time
	private int mLines = 0;  //first time
	private int mLinesG = 0; //kill by Gravity
	public int num;
	private int CubeSize;
    SharedPreferences prefs;
    private Context mCont;

	public Falling(int n, int m, int _size, Context context) {
		CubeSize = _size;
		mCont = context;
      	Mtrx = new int[n][m];
		Color = new int[n][m];
		MtrxS = new int[n][m];
		ColorS = new int[n][m];
		
		N = n;
		M = m;


        for (int i = 0; i < n; i++) {
           for (int j = 0; j < m; j++) {
               Mtrx[i][j] = 0;
                }
            }


		
	}

    //save Data Falling
    public void saveFalling() {
        SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(mCont);
        SharedPreferences.Editor editor = sharedPref.edit();
        for(int i = 0; i < N; i++) {
            for(int j = 0; j < M; j++) {
                editor.putInt("FallingMatrix" + i + "" + j , Mtrx[i][j]);
            }
        }
        editor.commit();
    }


    public void restoreFalling() {
        int check = 0;
        for(int i = 0; i < N; i++) {
            for(int j = 0; j < M; j++) {
                Mtrx[i][j] = prefs.getInt("FallingMatrix" + i + "" + j, 0);
                check = Mtrx[i][j];
            }
        }
    }



    public Falling() {
		
	}
	
    public int getKoef() {
        return Koef;
    }

    public void setNewGravLine() {
        mLinesG += getKoef();
    }
    public void setNewUsualLine() {

        mLines+=getKoef();
    }

    public void refreshLines() {
        mLinesG = 0;
        mLines = 0;
    }

    public int getGravLine() {
        return mLinesG;
    }
    public int getUsualLine() {
        return mLines;
    }

    public int getQtyFilledRows() {
        int qty = 0;
        for (int j = 0; j < M; j++) {
            for (int i = 0; i < N; i++) {
                if (Mtrx[i][j] == 1) {
                    return M - j;
                }
            }
        }
        return qty;
    }
	
	
	public void setParams(int n, int m, int _size, Context context) {
		CubeSize = _size;
        mCont = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(mCont);
		if((N > 0) || (M > 0)) return; //если уже была установка поля 
		Mtrx = new int[n][m];
		Color = new int[n][m];
		MtrxS = new int[n][m];
		ColorS = new int[n][m];
		N = n;
		M = m;
        if(prefs.getInt("needRefreshFalling", 0) == 1) {
            restoreFalling();
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("needRefreshFalling", 0);
            editor.commit();
        } else {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    Mtrx[i][j] = 0;
                }
            }
        }
	}
	

	

	public boolean checkFall(Figure Fig) {
		for(int i = 0; i < 4; i++) {
			if (!(Mtrx[Fig.F[i][0]][Fig.F[i][1]] == 0)) {
				return true;
			}
		}
		return false;
	}

	public void modifOneBallMtrx(OneBallFigure Fig) {
		Color[Fig.F[0]][Fig.F[1]] = Fig.Color;
		Mtrx[Fig.F[0]][Fig.F[1]] = 1;
	}

    public void modifMtrx(Figure Fig) {
		for(int i = 0; i < 4; i++) {
			Color[Fig.F[i][0]][Fig.F[i][1]] = Fig.Color;
			Mtrx[Fig.F[i][0]][Fig.F[i][1]] = 1;
		}
    }

	public boolean checkFallBonus(BonusFigure Fig) {
		if (!(Mtrx[Fig.F[0]][Fig.F[1]] == 0))
			return true;
		if (!(Mtrx[Fig.S[0]][Fig.S[1]] == 0))
			return true;
		if (!(Mtrx[Fig.T[0]][Fig.T[1]] == 0))
			return true;
		if (!(Mtrx[Fig.Fr[0]][Fig.Fr[1]] == 0))
			return true;
		if (!(Mtrx[Fig.F2[0]][Fig.F2[1]] == 0))
			return true;
		if (!(Mtrx[Fig.S2[0]][Fig.S2[1]] == 0))
			return true;
		return false;
	}



	public boolean checkFallOne(OneBallFigure Fig) {
        boolean check = false;
        Log.d("fall One", " X -- Y " + Fig.F[0] + " " + Fig.F[1]);
		if (!(Mtrx[Fig.F[0]][Fig.F[1]] == 0)) { //if intersect
            check = true;
            for (int i = Fig.F[1]; i < M; i++) {
                Log.d("fall One Seacrh free", " X -- Y " + Fig.F[0] + " " + i);
                if(Mtrx[Fig.F[0]][i] == 0) {
                    check = false;
                }

            }
        }

		return check;
	}

	public void modifMtrxBonus(BonusFigure Fig) {
		Color[Fig.F[0]][Fig.F[1]] = Fig.Color;
		Color[Fig.S[0]][Fig.S[1]] = Fig.Color;
		Color[Fig.T[0]][Fig.T[1]] = Fig.Color;
		Color[Fig.Fr[0]][Fig.Fr[1]] = Fig.Color;
		Color[Fig.F2[0]][Fig.F2[1]] = Fig.Color;
		Color[Fig.S2[0]][Fig.S2[1]] = Fig.Color;
		Mtrx[Fig.F[0]][Fig.F[1]] = 1;
		Mtrx[Fig.S[0]][Fig.S[1]] = 1;
		Mtrx[Fig.T[0]][Fig.T[1]] = 1;
		Mtrx[Fig.Fr[0]][Fig.Fr[1]] = 1;
		Mtrx[Fig.F2[0]][Fig.F2[1]] = 1;
		Mtrx[Fig.S2[0]][Fig.S2[1]] = 1;
	}

	public boolean check(int max) {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				MtrxS[i][j] = Mtrx[i][j];
				ColorS[i][j] = Color[i][j];
			}
		}
		Log.d("Dima", "Maz  " + max);
		int j = max, t = max, i, z = 0, n, flag = 0;
		for (n = 4; n > 0; n--) {
			j = t;
			if(j < 0) break;
			for (i = 0; i < N; i++) {
				Log.d("Dima", "Matrix " + i + " " + j);
				if (Mtrx[i][j] == 0) {
					break;
				}
			}
			if (i == N)
				i--;
			if (Mtrx[i][j] == 0) {
				t--;
				continue;
			}
			if (flag == 0) {
				num = j;
				flag = 1;
			}
			z++;
			for (int k = t; k >= 1; k--) {
				for (i = 0; i < N; i++) {
					Mtrx[i][k] = Mtrx[i][k - 1];
					Color[i][k] = Color[i][k - 1];
				}
			}
		}
		if (z == 0) {
			System.out.println("3");
			return false;
		} else {
            Koef  = z;
			System.out.println("4");
		    return true;
		}

	}

	public void set(int S) {
		CubeSize = S;
	}
	

	public boolean fallGravity(int j, int[] vect) {
		boolean b = false;
		if (j == M - 1)
			return false;
		for (int i = 0; i < N; i++) {
			if (((Mtrx[i][j] == 1) && (Mtrx[i][j + 1] == 0)) && (vect[i] == 0)) {
				b = true;
				for (int k = j + 1; k >= 1; k--) {
					Mtrx[i][k] = Mtrx[i][k - 1];
					Color[i][k] = Color[i][k - 1];
				}
			} else
				vect[i] = 1;
		}
		return b;
	}

	public void paint(Canvas canvas, Paint mpaint, Context context) {
		Bitmap mImage1;
		mImage1 = BallFactory.blackImage;
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		int numTheme = Integer// Number of old theme
				.valueOf(prefs.getString("ballThemes", "6"));
		long tim = System.currentTimeMillis();
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				if (Mtrx[i][j] == 1) {
					if (numTheme == 6) {
						switch (Color[i][j]) {
						case (0):
							mImage1 = BallFactory.blackImage;
							break;
						case (1):
							mImage1 = BallFactory.purpleImage;
							break;
						case (2):
							mImage1 = BallFactory.orangeImage;
							break;
						case (3):
							mImage1 = BallFactory.redImage;
							break;
						case (4):
							mImage1 = BallFactory.yellowImage;
							break;
						case (5):
							mImage1 = BallFactory.blueImage;
							break;
						}
					} else {
						switch (numTheme) {
						case (0):
							mImage1 = BallFactory.blackImage;
							break;
						case (1):
							mImage1 = BallFactory.blueImage;
							break;
						case (2):
							mImage1 = BallFactory.orangeImage;
							break;
						case (3):
							mImage1 = BallFactory.redImage;
							break;
						case (4):
							mImage1 = BallFactory.yellowImage;
							break;
						case (5):
							mImage1 = BallFactory.purpleImage;
							break;
						}
					}
					canvas.drawBitmap(mImage1, i * CubeSize, j * CubeSize, null);

				}
			}

		}
		long time3 = System.currentTimeMillis() - tim;
		Log.d("Dima", "TIME FSGSGSG " + time3);
	}

	public void paintS(Canvas canvas, Paint mpaint, Context context) {
		Bitmap mImage1;
		mImage1 = BallFactory.blackImage;
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);

		int numTheme = Integer// Number of old theme
				.valueOf(prefs.getString("ballThemes", "6"));
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				if (MtrxS[i][j] == 1) {
					if (numTheme == 6) {
						switch (ColorS[i][j]) {
						case (0):
							mImage1 = BallFactory.blackImage;
							break;
						case (1):
							mImage1 = BallFactory.purpleImage;
							break;
						case (2):
							mImage1 = BallFactory.orangeImage;
							break;
						case (3):
							mImage1 = BallFactory.redImage;
							break;
						case (4):
							mImage1 = BallFactory.yellowImage;
							break;
						case (5):
							mImage1 = BallFactory.blueImage;
							break;
						}
					} else {
						switch (numTheme) {
						case (0):
							mImage1 = BallFactory.blackImage;
							break;
						case (1):
							mImage1 = BallFactory.blueImage;
							break;
						case (2):
							mImage1 = BallFactory.orangeImage;
							break;
						case (3):
							mImage1 = BallFactory.redImage;
							break;
						case (4):
							mImage1 = BallFactory.yellowImage;
							break;
						case (5):
							mImage1 = BallFactory.purpleImage;
							break;
						}
					}

					canvas.drawBitmap(mImage1, i * CubeSize, j * CubeSize, null);

				}
			}

		}

	}
}
