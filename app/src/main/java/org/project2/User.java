package org.project2;

public class User implements Comparable {
	public int score;
	public String key_score;
	public String key_name;
	public String name;

	public User(int _score, String _name, String _key_score, String _key_name) {
		this.score = _score;
		this.name = _name;
		this.key_name = _key_name;
		this.key_score = _key_score;
	}

	/* ���������� ������ compareTo */

	public int compareTo(Object obj) {
		User tmp = (User) obj;
		if (this.score < tmp.score) {
			/* ������� ������ ����������� */
			return 1;
		} else if (this.score > tmp.score) {
			/* ������� ������ ����������� */
			return -1;
		}
		/* ������� ����� ����������� */
		return 0;
	}

}