package org.project2;

import java.util.Arrays;

import org.project3.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class TableRecords extends Activity implements OnClickListener {
	/** Called when the activity is first created. */
	public static final String user = "cols";
	public static final String scores = "";
	public static final String PREFS_NAME = "MyPrefsFile";
	private final String USER1 = "aay";
	private final String USER2 = "bby";
	private final String USER3 = "ccy";
	private final String USER4 = "ddy";
	private final String USER5 = "eey";
	private final String USER6 = "ffy";
	private final String USER7 = "lly";
	private final String USER8 = "mmy";
	private final String USER9 = "zzy";
	private final String USER_1 = "_aay";
	private final String USER_2 = "_bby";
	private final String USER_3 = "_ccy";
	private final String USER_4 = "_ddy";
	private final String USER_5 = "_eey";
	private final String USER_6 = "_ffy";
	private final String USER_7 = "_lly";
	private final String USER_8 = "_mmy";
	private final String USER_9 = "_zzy";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// Bundle extras = getIntent().getExtras();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.table);
		final WebView gamesTable = (WebView) findViewById(R.id.table_list);
		StringBuilder sb = new StringBuilder();
		WebViewUtils.loadTemplateFile(this, sb, "table_record.html");
		User[] users = new User[9];
		Bundle extras = getIntent().getExtras();
		String str;
		int score;
		str = extras.getString(user);
		score = extras.getInt(scores);

		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(this);
		SharedPreferences.Editor prefsEditor = settings.edit();
		users[0] = new User(settings.getInt(USER1, 900), settings.getString(
				USER_1, "Mike"), USER1, USER_1);
		users[1] = new User(settings.getInt(USER2, 800), settings.getString(
				USER_2, "Bill"), USER2, USER_2);
		users[2] = new User(settings.getInt(USER3, 700), settings.getString(
				USER_3, "Fill"), USER3, USER_3);
		users[3] = new User(settings.getInt(USER4, 600), settings.getString(
				USER_4, "James"), USER4, USER_4);
		users[4] = new User(settings.getInt(USER5, 500), settings.getString(
				USER_5, "Andrew"), USER5, USER_5);
		users[5] = new User(settings.getInt(USER6, 400), settings.getString(
				USER_6, "Kevin"), USER6, USER_6);
		users[6] = new User(settings.getInt(USER7, 300), settings.getString(
				USER_7, "Adam"), USER7, USER_7);
		users[7] = new User(settings.getInt(USER8, 200), settings.getString(
				USER_8, "Elizabet"), USER8, USER_8);
		users[8] = new User(settings.getInt(USER9, 100), settings.getString(
				USER_9, "Roy"), USER9, USER_9);
		Arrays.sort(users);
		if (score > users[8].score) {
			// delete users[0];
			prefsEditor.remove(users[8].key_score);
			prefsEditor.remove(users[8].key_name);
			prefsEditor.putString(users[8].key_name, str);
			prefsEditor.putInt(users[8].key_score, score);
			users[8] = new User(score, str, users[8].key_score,
					users[8].key_name);
			Arrays.sort(users);
		}
		prefsEditor.remove("bad1");
		prefsEditor.remove("good1");
		prefsEditor.putInt("bad1", users[8].score);
		prefsEditor.putInt("good1", users[0].score);
		prefsEditor.commit();
		sb.append("<div><h1>Table of records</h1></div>");
		sb.append("<div><table width=\"100%\" rules=\"rows\">");
		for (int i = 0; i < 9; i++) {
			sb.append("<tr><td>");
			sb.append(i + 1 + "." + users[i].name);
			sb.append("</td><td align=\"right\">");
			sb.append("" + users[i].score);
			sb.append("</td></tr>");
		}
		sb.append("</table></div>");
		sb.append("</body></html>");
		String summary = sb.toString();
		gamesTable.setWebViewClient(new WebViewClient());
		WebViewUtils.loadData(gamesTable, summary);
		Intent intent = new Intent();
		intent.setClass(this, TableRecords.class);
		intent.putExtra(TableRecords.scores, 0);
	}

	public void onClick(View v) {
		switch (v.getId()) {

		default:
			break;
		}
	}

}
