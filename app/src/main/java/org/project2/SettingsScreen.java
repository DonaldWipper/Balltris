/*import android.content.Context;
import android.content.SharedPreferences;*/
package org.project2;

import org.project3.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsScreen extends PreferenceActivity implements
		Preference.OnPreferenceChangeListener {
	/* Called when the activity is first created. */
    private static final String PREFERENCE_KEY = "seekBarPreference";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Load the preferences from an XML resource
		addPreferencesFromResource(R.xml.preferences);

		CheckBoxPreference vibration = (CheckBoxPreference) this
				.findPreference("pref_vibration");
		vibration.setOnPreferenceChangeListener(this);
		CheckBoxPreference sound = (CheckBoxPreference) this
				.findPreference("pref_sound");
		sound.setOnPreferenceChangeListener(this);
	}

	public boolean isValidFragment(String fragmentName) {
		return SettingsScreen.class.getName().equals(fragmentName);

	}


	public class CustomTextView extends TextView {

		public CustomTextView (Context context, AttributeSet attrs, int defStyle) {
			super(context, attrs, defStyle);
			init();
		}

		public CustomTextView(Context context, AttributeSet attrs) {
			super(context, attrs);
			init();
		}

		public CustomTextView(Context context) {
			super(context);
			init();
		}

		private void init() {
			Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
					"fonts/Economica-Regular.ttf");
			setTypeface(tf);
		}

	}


	public void onStop() {
        super.onStop();
        SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("Settings changed", 1);
        editor.commit();


    }


	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals(PREFERENCE_KEY)) {
			// Notify that value was really changed
			int value = sharedPreferences.getInt(PREFERENCE_KEY, 0);
			Toast.makeText(this, getString(R.string.summary, value), Toast.LENGTH_LONG).show();
		}
	}

	public boolean onPreferenceChange(Preference preference, Object newValue) {

		return true;
	}
}