package org.project2;

import org.project3.R;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.Matrix;
import 	android.util.Config;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

import static android.content.ContentValues.TAG;

class BallFactory {
    static public Bitmap blackImage = null;
    static public Bitmap blueImage = null;
    static public Bitmap orangeImage = null;
    static public Bitmap redImage = null;
    static public Bitmap yellowImage = null;
    static public Bitmap purpleImage = null;
    static public Bitmap next_t;
    static public Bitmap next_r;
    static public Bitmap next_s;
    static public Bitmap next_i;
    static public Bitmap next_o;
    static public Bitmap next_u;
    static public Bitmap next_l;

    static public Drawable mImageRight;
    static public Drawable mImageLeft;
    static public Drawable mImageDown;
    static public Drawable mImageDownPress;
    static public Drawable mImageRotate;
    static public Drawable mImageRotatePress;
    static public Drawable mImageRightPress;
    static public Drawable mImageLeftPress;
    static public Drawable mImagePauseStp;
    static public Drawable mImagePausePl;
    static public Bitmap mImageBack = null;
    static public Bitmap mImageWall = null;
    static public Bitmap mImageGoalFrame = null;  //Frame on tv
    static public Drawable mImageSettings = null;
    static public Drawable mImageMusicPlay = null;
    static public Drawable mImageMusicStop = null;
    static public Drawable mImage[];
    static public Resources res;
    static public int curImage = 1;
    static public int curImageFrame = 0;
    static public AssetManager mAssetManager;
    static public int mQtyGoals = 5;
    static public int mQtyFrames = 26;
    static public int mCurGoal = 0;
    public static double mCurScale = 1;
    public static boolean isAnimate = false; //0 -- off, 1-- animate now, 2 -- finish
    public static int mQtyMotions = 8;
    public static double mStartScale = 0.1;
    public static double mDeltaScale;
    public static long mStartTime;
    public static long mSpeedPerFrame;
    public static long mTimeRenderLastFrame;
    public static boolean isAnimateGoal = false;

    BallFactory(Context context) {
        res = context.getResources();
        mAssetManager = context.getAssets();
        blackImage = BitmapFactory.decodeResource(res, R.drawable.ball_black);
        blueImage = BitmapFactory.decodeResource(res, R.drawable.ball_blue);
        orangeImage = BitmapFactory.decodeResource(res, R.drawable.ball_orange);
        redImage = BitmapFactory.decodeResource(res, R.drawable.ball_red);
        yellowImage = BitmapFactory.decodeResource(res, R.drawable.ball_yellow);
        purpleImage = BitmapFactory.decodeResource(res, R.drawable.ball_purple);
        next_t = BitmapFactory.decodeResource(res, R.drawable.fig_t);
        next_r = BitmapFactory.decodeResource(res, R.drawable.fig_r);
        next_s = BitmapFactory.decodeResource(res, R.drawable.fig_s);
        next_i = BitmapFactory.decodeResource(res, R.drawable.fig_i);
        next_o = BitmapFactory.decodeResource(res, R.drawable.fig_o);
        next_u = BitmapFactory.decodeResource(res, R.drawable.fig_u);
        next_l = BitmapFactory.decodeResource(res, R.drawable.fig_l);

        //mImageWall = BitmapFactory.decodeResource(res, R.drawable.grass);
        //mImagePhone = BitmapFactory.decodeResource(res, R.drawable.ground);
        //mImagePhone2 = BitmapFactory.decodeResource(res, R.drawable.ground2);
        //mImagePhone3 = BitmapFactory.decodeResource(res, R.drawable.ground3);

        mImage = new Drawable[3];
        mImage[0] = res.getDrawable(R.drawable.fire0);
        mImage[1] = res.getDrawable(R.drawable.fire1);
        mImage[2] = res.getDrawable(R.drawable.fire2);

        mImageRight = res.getDrawable(R.drawable.right);
        mImageLeft = res.getDrawable(R.drawable.left);
        mImageDown = res.getDrawable(R.drawable.down);
        mImageRotate = res.getDrawable(R.drawable.round);
        mImageDownPress = res.getDrawable(R.drawable.down_press);
        mImageRotatePress = res.getDrawable(R.drawable.round_press);
        mImageRightPress = res.getDrawable(R.drawable.right_press);
        mImageLeftPress = res.getDrawable(R.drawable.left_press);
        mImagePauseStp = res.getDrawable(R.drawable.p);
        mImagePausePl = res.getDrawable(R.drawable.d);
        mImageSettings = res.getDrawable(R.drawable.settngs);
        mImageMusicPlay = res.getDrawable(R.drawable.music_on);
        mImageMusicStop = res.getDrawable(R.drawable.music_off);
    }

    public static void  createAnimation (long duration, double minScale) {
        mSpeedPerFrame = duration / mQtyMotions;
        mStartTime =  System.currentTimeMillis();
        mStartScale = minScale;
        mDeltaScale = (1.0 - mStartScale) / mQtyMotions;
        mCurScale = mStartScale;
        mStartScale = System.currentTimeMillis();
        isAnimate = true;

    }

    public  static void AnimatePreview(int reqWidth, int reqHeight) {
        if (System.currentTimeMillis() - mStartTime > mSpeedPerFrame) {
            if(mCurScale > 1.0) {
               mCurScale = 1.0;
               isAnimate = false;
            }

            setNewBackFrame((int) Math.round(reqWidth * mCurScale), (int) Math.round((reqHeight * mCurScale)));
            mCurScale += mDeltaScale;
            mStartTime = System.currentTimeMillis();
        }

    }


    public  static void AnimateEnd(int reqWidth, int reqHeight) {
        if (System.currentTimeMillis() - mStartTime > mSpeedPerFrame) {
            if(mCurScale < mStartScale) {
                mCurScale = mStartScale;
                isAnimate = false;
            }

            setNewBackFrame((int) Math.round(reqWidth * mCurScale), (int) Math.round((reqHeight * mCurScale)));
            mCurScale -= mDeltaScale;
            mStartTime = System.currentTimeMillis();
        }

    }

    public static  void AnimateGoal(int reqWidth, int reqHeight) {
        if (System.currentTimeMillis() -  mTimeRenderLastFrame  > Constants.DelayFrame) {
            BallFactory.setNewImageFrame();
            BallFactory.setNewBackFrame(reqWidth, reqHeight);
            if (BallFactory.curImageFrame == 0) {
                isAnimateGoal = false;
                mTimeRenderLastFrame = System.currentTimeMillis();
                //lastTimeFrame = 0;
            }
            mTimeRenderLastFrame = System.currentTimeMillis();
        }
    }


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float)height / (float)reqHeight);
            } else {
                inSampleSize = Math.round((float)width / (float)reqWidth);
            }
        }
        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        Bitmap pic = null;
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        pic = BitmapFactory.decodeResource(res, resId, options);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(pic, reqWidth, reqHeight, true);
        pic.recycle();
        return scaledBitmap;
    }



    public static Bitmap scaleBitmap(Bitmap input, String path, int ResId, int wantedWidth, int wantedHeight) {
        if(input != null) {
            input.recycle();
        }
        InputStream is= null;
        if(path == null) {
            input = BitmapFactory.decodeResource(res, ResId);
        } else {
            try {
                is = mAssetManager.open(path);
                input = BitmapFactory.decodeStream(is);
                is.close();
            }
            catch(IOException ex){
                Log.d ("Error", ex.toString());
            }

        }
        Bitmap output = Bitmap.createBitmap(wantedWidth, wantedHeight, Bitmap.Config.ARGB_8888);
        Log.d("Dima", "wanted bitmap  " + wantedWidth + "  " + wantedHeight );
        Canvas canvas = new Canvas(output);
        Matrix m = new Matrix();
        m.setScale((float) wantedWidth / input.getWidth(), (float) wantedHeight / input.getHeight());
        canvas.drawBitmap(input, m, new Paint());
        input.recycle();
        return  output;
    }



    public  static void setNewGoal () {
        mCurGoal++;
    }
    public  static void setNewImageFrame () {
        curImageFrame = (curImageFrame + 1) % mQtyFrames;
    }
    public  static void setGoal (int goal) {mCurGoal = goal;};
    public  static int  getGoal () { return mCurGoal;}

    public static  void  refreshImages() {
        curImage = 1;
        mCurGoal = 0;
    }

    public static int setNewBackFrame(int wantedWidth, int wantedHeight) {

        if(mImageGoalFrame  != null) {
            mImageGoalFrame.recycle();
        }
        //4 гола
        //по 6 слайдо

        String curGoal = mCurGoal % mQtyGoals   + "";
        String curFrame = curImageFrame  + "";
        String fileName = "goals/goal" +  curGoal + "/goal" + curFrame + ".jpg";
        Log.d("Filename frame", fileName);

        mImageGoalFrame  = scaleBitmap(mImageGoalFrame, fileName, 0, wantedWidth, wantedHeight);
        return 0;
    }


    public void makeFree()  {
        curImage = 0;
        curImageFrame = 0;
        blackImage.recycle();
        blueImage.recycle();
        orangeImage.recycle();
        redImage.recycle();
        yellowImage.recycle();
        purpleImage.recycle();
        next_t.recycle();
        next_r.recycle();
        next_s.recycle();
        next_i.recycle();
        next_o.recycle();
        next_u.recycle();
        next_l.recycle();
        blackImage.recycle();
        blueImage.recycle();
        orangeImage.recycle();
        redImage.recycle();
        yellowImage.recycle();
        purpleImage.recycle();
        next_t.recycle();
        next_r.recycle();
        next_s.recycle();
        next_i.recycle();
        next_o.recycle();
        next_u.recycle();
        next_l.recycle();
        mImageBack.recycle();
        mImageGoalFrame.recycle();
        mImageWall.recycle();




    }
}
