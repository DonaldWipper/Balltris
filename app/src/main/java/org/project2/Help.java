package org.project2;

import org.project3.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Help extends Activity implements OnClickListener {
	/** Called when the activity is first created. */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// Bundle extras = getIntent().getExtras();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help);
		final WebView About = (WebView) findViewById(R.id.help);
		StringBuilder sb = new StringBuilder();
		WebViewUtils.loadTemplateFile(this, sb, "help.html");
		String summary = sb.toString();
		About.setWebViewClient(new WebViewClient());
		WebViewUtils.loadData(About, summary);
	}

	public void onClick(View v) {
		switch (v.getId()) {

		default:
			break;
		}
	}

}
