package org.customlistpreference;

import java.util.ArrayList;

import org.project2.SettingsScreen;
import org.project2.StartScreen2;
import org.project3.R;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.ListPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

public class CustomListPreference extends ListPreference {
	CustomListPreferenceAdapter customListPreferenceAdapter = null;
	Context mContext;
	private LayoutInflater mInflater;
	CharSequence[] entries;
	String keyValue;
	ArrayList<RadioButton> rButtonList;
	SharedPreferences prefs;
	SharedPreferences.Editor editor;

	public CustomListPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		mInflater = LayoutInflater.from(context);
		rButtonList = new ArrayList<RadioButton>();
		prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		editor = prefs.edit();
	}

	@Override
	protected void onPrepareDialogBuilder(Builder builder) {
		keyValue = getKey();
		entries = getEntries();
		customListPreferenceAdapter = new CustomListPreferenceAdapter(mContext);
		if (entries == null) {
			throw new IllegalStateException(
					"ListPreference requires an entries array not empty");
		}

		builder.setAdapter(customListPreferenceAdapter,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

					}
				});
	}

	private class CustomListPreferenceAdapter extends BaseAdapter {
		public CustomListPreferenceAdapter(Context context) {

		}

		public int getCount() {
			return entries.length;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View row = convertView;
			if ((row == null) || (row.getId() != position)) {
				row = mInflater.inflate(R.layout.custom_list_preference_row,
						null);
				CustomHolder holder = new CustomHolder(row, position);
				row.setTag(holder);
				row.setClickable(true);
				row.setId(position);
				row.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						for (RadioButton rb : rButtonList) {
							if (rb.getId() == position) {
								rb.setChecked(true);
								editor.putString(keyValue, "" + position);
								editor.commit();
							} else {
								rb.setChecked(false);
							}
						}
					}
				});

			}
			return row;
		}

		class CustomHolder {
			private TextView text = null;
			private ImageView image = null;
			private RadioButton rButton = null;

			CustomHolder(View row, int position) {

				text = (TextView) row
						.findViewById(R.id.custom_list_view_row_text_view);
				Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
						"fonts/Economica-Regular.ttf");
				text.setTypeface(tf);
				text.setText(entries[position]);
				image = (ImageView) row
						.findViewById(R.id.custom_list_view_row_image_view);
				switch (position) {
				case (0):
					image.setImageResource(R.drawable.ball_black);
					break;
				case (1):
					image.setImageResource(R.drawable.ball_blue);
					break;
				case (2):
					image.setImageResource(R.drawable.ball_orange);
					break;
				case (3):
					image.setImageResource(R.drawable.ball_red);
					break;
				case (4):
					image.setImageResource(R.drawable.ball_yellow);
					break;
				case (5):
					image.setImageResource(R.drawable.ball_purple);
					break;
				}
				rButton = (RadioButton) row
						.findViewById(R.id.custom_list_view_row_radio_button);
				int numChecked = Integer// Number of old theme
						.valueOf(prefs.getString(keyValue, "6"));
				if (position == numChecked) {
					rButton.setChecked(true);// Set checked type of old theme
				}
				rButton.setId(position);
				rButton.setClickable(false);
				rButtonList.add(rButton);
			}
		}
	}
}
