package org.online_table;

import java.util.Arrays;

import org.project2.User;
import org.project3.R;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import java.math.BigDecimal;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import android.util.Log;
import android.widget.Button;

import org.project3.R;
import com.shephertz.app42.paas.sdk.android.App42Exception;
import com.shephertz.app42.paas.sdk.android.game.Game;

public class TableWeb extends Activity implements AsyncApp42ServiceApi.App42ScoreBoardServiceListener, OnClickListener  {
	
/** The async service. */
	private AsyncApp42ServiceApi asyncService;

	/** The progress dialog. */
	private ProgressDialog progressDialog;
    private WebView gamesTable;
    public static String Name = "cols";
    public static String Score = "";
	public static String Mode; //0-show offline mode, 1-online
    private  int mScore = 0;
	private  String UserName = "";

    public static final String PREFS_NAME = "MyPrefsFile";
    private final String USER1 = "aay";
    private final String USER2 = "bby";
    private final String USER3 = "ccy";
    private final String USER4 = "ddy";
    private final String USER5 = "eey";
    private final String USER6 = "ffy";
    private final String USER7 = "lly";
    private final String USER8 = "mmy";
    private final String USER9 = "zzy";
    private final String USER_1 = "_aay";
    private final String USER_2 = "_bby";
    private final String USER_3 = "_ccy";
    private final String USER_4 = "_ddy";
    private final String USER_5 = "_eey";
    private final String USER_6 = "_ffy";
    private final String USER_7 = "_lly";
    private final String USER_8 = "_mmy";
    private final String USER_9 = "_zzy";

	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		asyncService = AsyncApp42ServiceApi.instance(this);
                setContentView(R.layout.table);
		gamesTable = (WebView) findViewById(R.id.table_list);

        Button onlineButton = (Button) findViewById(R.id.btn_online_res);
        onlineButton.setOnClickListener(this);

        Button offlineButton = (Button) findViewById(R.id.btn_offline_res);
        offlineButton.setOnClickListener(this);
		Bundle extras = getIntent().getExtras();
		UserName  = extras.getString(Name);
		mScore = extras.getInt(Score);
		int mode = extras.getInt(Mode);
		if(mode == 1) {
			progressDialog = ProgressDialog.show(this, "", "Saving Score..");
			progressDialog.setCancelable(true);
			asyncService.saveScoreForUser(Constants.App42GameName, UserName, new BigDecimal(mScore), this);
			progressDialog.dismiss();
			asyncService.getLeaderBoard(Constants.App42GameName, 50, this);

		} else {
			showOffline();
		}
	
	}
	
	/**
	 * On previous clicked.
	 *
	 * @param view the view
	 */
	public void onPreviousClicked(View view) {
		Intent mainIntent = new Intent(this, UserSample.class);
		this.startActivity(mainIntent);
	}
	
	/**
	 * On next clicked.
	 *
	 * @param view the view
	 */
	public void onNextClicked(View view) {
		Intent mainIntent = new Intent(this, org.project2.StartScreen2.class);
		this.startActivity(mainIntent);
	}
	
	/**
	 * On get score clicked.
	 *
	 * @param viewe the viewe
	 */
	public void onGetScoreClicked(View viewe){
		progressDialog = ProgressDialog.show(this, "", "Loading..");
		progressDialog.setCancelable(true);
		asyncService.getLeaderBoard(Constants.App42GameName, 50, this);

	}
	
	/**
	 * On save score clicked.
	 *
	 * @param viewe the viewe
	 */
	public void onSaveScoreClicked(View viewe){
		progressDialog = ProgressDialog.show(this, "", "Saving Score..");
		progressDialog.setCancelable(true);
		asyncService.saveScoreForUser(Constants.App42GameName, Constants.UserName, new BigDecimal(10000), this);
	}
	
	/* (non-Javadoc)
	 * @see com.app42.android.sample.AsyncApp42ServiceApi.App42ScoreBoardServiceListener#onSaveScoreSuccess(com.shephertz.app42.paas.sdk.android.game.Game)
	 */
	@Override
	public void onSaveScoreSuccess(Game response) {
		// TODO Auto-generated method stub
		progressDialog.dismiss();
		createAlertDialog("Score SuccessFully Saved, For UserName : "+ response.getScoreList().get(0).getUserName()
				+  " With Score : " + response.getScoreList().get(0).getValue());
	}

	/* (non-Javadoc)
	 * @see com.app42.android.sample.AsyncApp42ServiceApi.App42ScoreBoardServiceListener#onSaveScoreFailed(com.shephertz.app42.paas.sdk.android.App42Exception)
	 */
	@Override
	public void onSaveScoreFailed(App42Exception ex) {
		// TODO Auto-generated method stub
		progressDialog.dismiss();
		createAlertDialog("Exception Occurred : "+ ex.getMessage());
	}

	/* (non-Javadoc)
	 * @see com.app42.android.sample.AsyncApp42ServiceApi.App42ScoreBoardServiceListener#onLeaderBoardSuccess(com.shephertz.app42.paas.sdk.android.game.Game)
	 */
	@Override
	public void onLeaderBoardSuccess(Game response) {
		// TODO Auto-generated method stub


        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        org.project2.WebViewUtils.loadTemplateFile(this, sb, "index.html");

        gamesTable.setWebViewClient(new WebViewClient());

		//progressDialog.dismiss();
        int ListSize =  response.getScoreList().size();
        sb.append("var test = new BFG.Leaderboard({  interval:5,");
        sb.append("margin:5,");
        sb.append("sort:'count',");
        sb.append("max:" + 23 + ",");
        sb.append("display:function(item){\n" +
                "\t\tvar \tcontent = document.createElement('div'),\n" +
                "\t\t\ta = content.appendChild(document.createElement('a')),\n" +
                "\t\t\tspan = document.createElement('span');\n" +
                "\t\ta.innerHTML = item.title;\n" +
                "\t\ta.href = \"#\";\n" +
                "\t\tspan.innerHTML = item.count;\n" +
                "\t\ta.appendChild(span);\n" +
                "\t\treturn content;\n" +
                "\t},\n" +
                "\t");
        sb.append("dataCallback:function(){ return [");

		for(int i = 0; i < 23; i++){
			Log.d("Dima", "Scorel " + i);
			int rank = i+1;
            String name = response.getScoreList().get(i).getUserName();
            BigDecimal scoreValue = response.getScoreList().get(i).getValue();
            sb2.append("{id:" + rank + ",title:" + "\"" + rank + ". " + name + "\",");
			//Log.d("Dima","{id:" + rank + ",title:" + "\"" + rank + ". " + name + "\"," + " count:" + scoreValue +"}");
			sb2.append(" count:" + scoreValue +"}");
		    if(i != ListSize - 1) {
                sb2.append(",");
            }
 		}
        sb2.append("];");
		sb2.append(" } }); test.start(); </script>  + </body> + </html>");
        String summary = sb.toString() + sb2.toString();
        org.project2.WebViewUtils.loadData(gamesTable, summary);
        gamesTable.getSettings().setJavaScriptEnabled(true);
        gamesTable.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        progressDialog.dismiss();
	}

    public void showOffline() {
        // TODO Auto-generated method stub
        StringBuilder sb = new StringBuilder();
        User[] users = new User[9];
        Bundle extras = getIntent().getExtras();


        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(this);
        SharedPreferences.Editor prefsEditor = settings.edit();
        users[0] = new User(settings.getInt(USER1, 900), settings.getString(
                USER_1, "Mike"), USER1, USER_1);
        users[1] = new User(settings.getInt(USER2, 800), settings.getString(
                USER_2, "Bill"), USER2, USER_2);
        users[2] = new User(settings.getInt(USER3, 700), settings.getString(
                USER_3, "Fill"), USER3, USER_3);
        users[3] = new User(settings.getInt(USER4, 600), settings.getString(
                USER_4, "James"), USER4, USER_4);
        users[4] = new User(settings.getInt(USER5, 500), settings.getString(
                USER_5, "Andrew"), USER5, USER_5);
        users[5] = new User(settings.getInt(USER6, 400), settings.getString(
                USER_6, "Kevin"), USER6, USER_6);
        users[6] = new User(settings.getInt(USER7, 300), settings.getString(
                USER_7, "Adam"), USER7, USER_7);
        users[7] = new User(settings.getInt(USER8, 200), settings.getString(
                USER_8, "Elizabet"), USER8, USER_8);
        users[8] = new User(settings.getInt(USER9, 100), settings.getString(
                USER_9, "Roy"), USER9, USER_9);
        Arrays.sort(users);
        if (mScore > users[8].score) {
            // delete users[0];
            prefsEditor.remove(users[8].key_score);
            prefsEditor.remove(users[8].key_name);
            prefsEditor.putString(users[8].key_name, UserName);
            prefsEditor.putInt(users[8].key_score, mScore);
            users[8] = new User(mScore, UserName, users[8].key_score,
                    users[8].key_name);
            Arrays.sort(users);
        }
        prefsEditor.remove("bad1");
        prefsEditor.remove("good1");
        prefsEditor.putInt("bad1", users[8].score);
        prefsEditor.putInt("good1", users[0].score);
        prefsEditor.commit();
        sb.append("<div><h1>Leaderboard</h1></div>");
        sb.append("<div><table width=\"100%\" rules=\"rows\">");
        for (int i = 0; i < 9; i++) {
            sb.append("<tr><td>");
            sb.append(i + 1 + "." + users[i].name);
            sb.append("</td><td align=\"right\">");
            sb.append("" + users[i].score);
            sb.append("</td></tr>");
        }
        sb.append("</table></div>");
        sb.append("</body></html>");
        String summary = sb.toString();
        gamesTable.setWebViewClient(new WebViewClient());

        org.project2.WebViewUtils.loadData(gamesTable, summary);
		mScore = 0;


    }

	/* (non-Javadoc)
	 * @see com.app42.android.sample.AsyncApp42ServiceApi.App42ScoreBoardServiceListener#onLeaderBoardFailed(com.shephertz.app42.paas.sdk.android.App42Exception)
	 */
	@Override
	public void onLeaderBoardFailed(App42Exception ex) {
		// TODO Auto-generated method stub
		progressDialog.dismiss();
		createAlertDialog("Exception Occurred : "+ ex.getMessage());
	}
	
	/**
	 * Creates the alert dialog.
	 *
	 * @param msg the msg
	 */
	public void createAlertDialog(String msg) {
		AlertDialog.Builder alertbox = new AlertDialog.Builder(
				TableWeb.this);
		alertbox.setTitle("Response Message");
		alertbox.setMessage(msg);
		alertbox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			// do something when the button is clicked
			public void onClick(DialogInterface arg0, int arg1) 
			{
			}
		});
		alertbox.show();
	}

	public void onClick(View v) {
		switch (v.getId()) {
            case R.id.btn_offline_res: {
                showOffline();
				break;
            }

            case R.id.btn_online_res: {
				progressDialog = ProgressDialog.show(this, "", "Load online table..");
				progressDialog.setCancelable(true);
                asyncService.getLeaderBoard(Constants.App42GameName, 50, this);
                break;
            }


		default:
			break;
		}
	}

}

