/**
 * -----------------------------------------------------------------------
 *     Copyright  2010 ShepHertz Technologies Pvt Ltd. All rights reserved.
 * -----------------------------------------------------------------------
 */
package org.online_table;

import java.math.BigDecimal;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.project2.StartScreen2;
import org.project3.R;


import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.shephertz.app42.paas.sdk.android.App42API;
import com.shephertz.app42.paas.sdk.android.App42CallBack;
import com.shephertz.app42.paas.sdk.android.App42Exception;
import com.shephertz.app42.paas.sdk.android.user.User;
import com.shephertz.app42.paas.sdk.android.user.UserService;

/**
 * The Class UserSample.
 */
public class UserSample extends Activity implements
        AsyncApp42ServiceApi.App42UserServiceListener {

    /**
     * The async service.
     */
    private AsyncApp42ServiceApi asyncService;
    private UserService userService;
    public Context cntx;
    /**
     * The progress dialog.
     */
    private ProgressDialog progressDialog;

    /**
     * The user name.
     */
    private String userName = "Utyan";

    /**
     * The password.
     */
    private String password = "pswd";
    private int mScore = 0;
    private int flgSgn = 0;
    private TextView txtView;
    private Button registerButton;
    private Button signButton;

    /**
     * The email.
     */
    private String email = "utiralov@yandex.ru";
    public static String Score;
    public static final String user = "cols";

    private static final int MY_PASSWORD_DIALOG_ID = 4; //add constant for password dialog
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user);
        Bundle extras = getIntent().getExtras();
        mScore = extras.getInt(Score);
        userName = extras.getString(user);
        registerButton = (Button) findViewById(R.id.RegisterButton);
        signButton = (Button) findViewById(R.id.SigninoutButton);


        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        userName = settings.getString("prevUser", "");  //last login
        password = settings.getString("Pswrd", "");  //last pswrd

        //если уже был залогинен
        if((userName != "") && (password != "")) {
            registerButton.setVisibility(View.INVISIBLE);
            signButton.setText("Sign out");
            flgSgn = 1;
        }

        txtView = (TextView) findViewById(R.id.TextView_UserName);
        txtView.setText(userName);

        asyncService = AsyncApp42ServiceApi.instance(this);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /**
     * On previous clicked.
     *
     * @param view the view
     */


    public void showDialogSgn() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.sign_in, null);
        final EditText name = (EditText) layout.findViewById(R.id.EditText_UserNameSgn);
        final EditText pswd = (EditText) layout.findViewById(R.id.EditText_UserPswdSgn);


        final TextView error = (TextView) layout.findViewById(R.id.TextView_PwdProblem);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Sign in bubatris");
        builder.setView(layout);
        cntx = this;


        builder
                .setCancelable(false)
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }

                            ;
                        }
                )
                .setPositiveButton("Go",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {
                                userName = name.getText().toString();;
                                password = pswd.getText().toString();
                                showDialog3();
                            }
                        }
         );

        // create alert dialog
        AlertDialog alertDialog = builder.create();

        // show it
        alertDialog.show();


    }

    public void showDialog() {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ;
        final View layout = inflater.inflate(R.layout.email_password, null);
        final EditText password1 = (EditText) layout.findViewById(R.id.EditText_Pwd1);
        final EditText password2 = (EditText) layout.findViewById(R.id.EditText_Pwd2);
        final EditText txtNickname = (EditText) layout.findViewById(R.id.EditText_Nickname);
        final EditText txtEmail = (EditText) layout.findViewById(R.id.EditText_email);

        final TextView error = (TextView) layout.findViewById(R.id.TextView_PwdProblem);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Register in bubatris");
        builder.setView(layout);
        cntx = this;


        // set dialog message
        builder
                .setCancelable(false)
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }

                            ;
                        }
                )
                .setPositiveButton("Go",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {
                                String strPassword1 = password1.getText().toString();
                                String strPassword2 = password2.getText().toString();
                                String strUser = txtNickname.getText().toString();
                                String strEmail = txtEmail.getText().toString();
                                userName = strUser;
                                password = strPassword1;
                                if (strPassword1.equals(strPassword2)) {
                                    error.setText("Great");
                                    userName = strUser;
                                    password = strPassword1;
                                    email = strEmail;
                                    Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                                    showDialog2();


                                }
                            }
                        }
                );
        // create alert dialog
        AlertDialog alertDialog = builder.create();

        // show it
        alertDialog.show();

    }

    public void showDialog3() {
        asyncService.authenticateUser(userName, password, this);
        SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("prevUser", userName);
        editor.putString("Pswrd", password);
        editor.commit();
    }

    public void showDialog2() {
        progressDialog = ProgressDialog.show(this, "", "registering..");
        progressDialog.setCancelable(true);
        asyncService.createUser(userName, password, email, this);
    }


    public void onPreviousClicked(View view) {
        Intent mainIntent = new Intent(this, StartScreen2.class);
        this.startActivity(mainIntent);
    }

    /**
     * On next clicked.
     *
     * @param view the view
     */
    public void onNextClicked(View view) {
        Intent mainIntent = new Intent(this, TableWeb.class);
        mainIntent.putExtra(TableWeb.Name, userName);
        mainIntent.putExtra(TableWeb.Score, mScore);
        mainIntent.putExtra(TableWeb.Mode, 1);
        Log.d("Q2UName2", userName);
        this.startActivity(mainIntent);

    }


    /**
     * On signin clicked.
     *
     * @param view the view
     */
    public void onSigninClicked(View view) {
        String txtStr = "signing in.. ";
        if(flgSgn == 1) {
            txtStr = "signing out..";
        }


        progressDialog = ProgressDialog.show(this, "", txtStr);
        progressDialog.setCancelable(true);

        if(flgSgn == 1) {
            SharedPreferences sharedPref = PreferenceManager
                    .getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("prevUser", "");
            editor.putString("Pswrd", "");
            userName = "";
            password = "";
            //txtView.setText(userName);
            editor.commit();
            registerButton.setVisibility(View.VISIBLE);
            signButton.setText("Sign in");
            txtView.setText("");
            flgSgn = 0;

        } else {
            showDialogSgn();
            //txtView.setText(userName);


        }
        progressDialog.dismiss();
    }

    /**
     * On register clicked.
     *
     * @param view the view
     */
    public void onRegisterClicked(View view) {
        showDialog();
        //progressDialog = ProgressDialog.show(this, "", "registering..");
        //progressDialog.setCancelable(true);
        //asyncService.createUser(userName, password, email, this);
    }

    /**
     * On get user clicked.
     *
     * @param view the view
     */
    public void onGetUserClicked(View view) {
        progressDialog = ProgressDialog.show(this, "", "Getting..");
        progressDialog.setCancelable(true);
        asyncService.getUser(userName, this);
    }

    /* (non-Javadoc)
	 * @see com.app42.android.sample.AsyncApp42ServiceApi.App42UserServiceListener#onUserCreated(com.shephertz.app42.paas.sdk.android.user.User)
	 */
    @Override
    public void onUserCreated(User user) {
        progressDialog.dismiss();
        SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("prevUser", userName);
        editor.putString("Pswrd", password);
        editor.commit();
        txtView.setText(userName);
        createAlertDialog("User Created :    UserName -> "
                + user.getUserName().toString() + "  Email ID -> "
                + user.getEmail());
    }

    /* (non-Javadoc)
	 * @see com.app42.android.sample.AsyncApp42ServiceApi.App42UserServiceListener#onUserAuthenticated(com.shephertz.app42.paas.sdk.android.user.User)
	 */
    @Override
    public void onUserAuthenticated(User response) {
        //progressDialog.dismiss();
        SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("prevUser", userName);
        editor.putString("Pswrd", password);
        editor.commit();
        txtView.setText(userName);
        registerButton.setVisibility(View.INVISIBLE);
        signButton.setText("Sign out");
        flgSgn = 1;
        createAlertDialog("SuccessFully Authenticated : UserName Is -> "
                + response.getUserName().toString());
    }

    /* (non-Javadoc)
	 * @see com.app42.android.sample.AsyncApp42ServiceApi.App42UserServiceListener#onCreationFailed(com.shephertz.app42.paas.sdk.android.App42Exception)
	 */
    @Override
    public void onCreationFailed(App42Exception exception) {
        // TODO Auto-generated method stub
        Toast.makeText(getApplicationContext(), "Creation of user was failed", Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();
        createAlertDialog(exception.getMessage());
    }

    /* (non-Javadoc)
	 * @see com.app42.android.sample.AsyncApp42ServiceApi.App42UserServiceListener#onAuthenticationFailed(com.shephertz.app42.paas.sdk.android.App42Exception)
	 */
    @Override
    public void onAuthenticationFailed(App42Exception exception) {
        // TODO Auto-generated method stub
        Toast.makeText(getApplicationContext(), "Authentication was failed", Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();
        createAlertDialog(exception.getMessage());
    }

    /**
     * Creates the alert dialog.
     *
     * @param msg the msg
     */
	/*
	 * used to create alert dialog.
	 */
    public void createAlertDialog(String msg) {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(UserSample.this);
        alertbox.setTitle("Response Message");
        alertbox.setMessage(msg);
        alertbox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            // do something when the button is clicked
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        alertbox.show();
    }

    /* (non-Javadoc)
	 * @see com.app42.android.sample.AsyncApp42ServiceApi.App42UserServiceListener#onGetUserSuccess(com.shephertz.app42.paas.sdk.android.user.User)
	 */
    @Override
    public void onGetUserSuccess(User response) {
        // TODO Auto-generated method stub
        String userName = response.getUserName();
        String email = response.getEmail();
        progressDialog.dismiss();
        createAlertDialog("User Name Is : " + userName + "EmailId Is : "
                + email);
    }

    /* (non-Javadoc)
	 * @see com.app42.android.sample.AsyncApp42ServiceApi.App42UserServiceListener#onGetUserFailed(com.shephertz.app42.paas.sdk.android.App42Exception)
	 */
    @Override
    public void onGetUserFailed(App42Exception exception) {
        // TODO Auto-generated method stub
        progressDialog.dismiss();
        createAlertDialog(exception.getMessage());
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("UserSample Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
