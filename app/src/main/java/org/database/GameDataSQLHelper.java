package org.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class GameDataSQLHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "GAME_DATABASE";
	private static final int DATABASE_VERSION = 4;
	// Table name
	public static final String TABLE_L1 = "score_table_level_one";
	public static final String TABLE_L2 = "game_table_level_two";
	public static final String TABLE_L3 = "score_table_level_tree";
	public static final String TABLE_L4 = "game_table_level_four";
	public static final String TABLE_L5 = "game_table_level_five";
	// Columns
	public static final String NAME = "player";
	public static final String SCORE = "result";
	public static final String SQL_CREATE_TABLE_L1 = "CREATE TABLE " + TABLE_L1
			+ " (_id integer primary key autoincrement, " + NAME + " TEXT, "
			+ SCORE + " TEXT); ";
	public static final String SQL_CREATE_TABLE_L2 = "CREATE TABLE " + TABLE_L2
			+ " (_id integer primary key autoincrement, " + NAME + " TEXT, "
			+ SCORE + " TEXT); ";
	public static final String SQL_CREATE_TABLE_L3 = "CREATE TABLE " + TABLE_L3
			+ " (_id integer primary key autoincrement, " + NAME + " TEXT, "
			+ SCORE + " TEXT); ";
	public static final String SQL_CREATE_TABLE_L4 = "CREATE TABLE " + TABLE_L4
			+ " (_id integer primary key autoincrement, " + NAME + " TEXT, "
			+ SCORE + " TEXT); ";
	public static final String SQL_CREATE_TABLE_L5 = "CREATE TABLE " + TABLE_L5
			+ " (_id integer primary key autoincrement, " + NAME + " TEXT, "
			+ SCORE + " TEXT); ";

	public GameDataSQLHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_TABLE_L1);
		db.execSQL(SQL_CREATE_TABLE_L2);
		db.execSQL(SQL_CREATE_TABLE_L3);
		db.execSQL(SQL_CREATE_TABLE_L4);
		db.execSQL(SQL_CREATE_TABLE_L5);
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.d("gdm", "onUpgrade");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_L1);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_L2);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_L3);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_L4);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_L5);
		onCreate(db);
	}

}